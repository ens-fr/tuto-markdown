# Créations d'élèves

Il s'agit de travaux d'élèves de première NSI.

Vous pouvez vous en inspirer.

- Il n'est pas recommandé d'inclure trop de mathématiques, c'est trop délicat pour débuter.
- Il est recommandé de ne pas souligner ; on le fait sur un cahier, pas sur un site web.

==Bravo à eux.==

Voici les [travaux 2022 sur un site dédié](https://ens-fr.gitlab.io/eleves-md/)

Les travaux 2021 sont sur les pages suivantes.
