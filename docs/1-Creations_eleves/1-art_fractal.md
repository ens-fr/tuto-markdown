# L'art fractal


## 🕮 Définition

> L'art fractal est ==une forme d'art algorithmique== 
qui consiste à produire des images, des animations 
et même des musiques à partir d'objets fractals.

Les [fractales](
https://fr.wikipedia.org/wiki/Fractale#Notes_et_r%C3%A9f%C3%A9rences
) sont des [objets mathématiques](
https://fr.wikipedia.org/wiki/Math%C3%A9matiques
) et géométriques « infiniment morcelés » dont des détails sont observables 
à une échelle arbitrairement choisie. En zoomant sur une partie de la figure, 
il est possible de retrouver toute la figure ; 
on dit alors qu’elle est « **auto similaire** ».

L'art fractal s'est développé à partir du milieu des années 1980. 
C'est un genre d'[art numérique](
https://fr.wikipedia.org/wiki/Art_num%C3%A9rique
).  
!!! example "Exemples"

    === "Motif mosquée"
        Des figures géométriques arabes telles que celle-ci 
        pourraient avoir préfiguré l'art fractal, comme sur le dôme principal 
        de la mosquée Selimiye à Edirne, en Turquie, 
        avec des motifs autosimilaires.

        ![dôme mosquée](
        https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/
        Selimiye_Mosque%2C_Dome.jpg/1024px-Selimiye_Mosque%2C_Dome.jpg
        )  

    === "Flocon de Koch" 
        [Flocon de Koch](https://fr.wikipedia.org/wiki/Flocon_de_Koch).  
        Une extension de la notion de dimension permet d'attribuer 
        à la courbe de Koch une dimension fractale (non entière) 
        dont la valeur est :  

        $$
        d = \frac{\ln 4} {\ln 3} \approx 1,26        
        $$  

        ![koch](
        https://upload.wikimedia.org/wikipedia/commons/b/bf/Koch_anime.gif
        )

    === "Ensemble de Mandelbrot"
        [Mandelbrot](https://fr.wikipedia.org/wiki/Ensemble_de_Mandelbrot) : 
        Suite de nombres complexes définie par récurrence par :  

        $$
        \begin{cases}
        z_{0} = 0 \\\
        z_{n+1} = z_{n}^2 + c
        \end{cases}
        $$

        ![mandelbrot](https://upload.wikimedia.org/wikipedia/commons/
        thumb/b/b5/Mandel_zoom_04_seehorse_tail.jpg/
        800px-Mandel_zoom_04_seehorse_tail.jpg)
        
    === "Ensemble de Julia"
        L'[ensemble de Julia](https://fr.wikipedia.org/wiki/Ensemble_de_Julia) 
        essentiellement caractérisé par le fait qu'une petite perturbation 
        au départ se répercute en un changement radical de cette suite (chaos).  
        Gaston Julia est le mathématicien français qui en est l'auteur 
        et il est à l'origine d'une nouvelle branche de mathématique : 
        la **dynamique holomorphe**.  

        ![Julia](https://upload.wikimedia.org/wikipedia/commons/1/17/
        Julia_set_%28highres_01%29.jpg)  

---

## 📣 Introduction

> L'ensemble de Julia, l'ensemble de Mandelbrot et maintenant le [Mandelbulb](
https://fr.wikipedia.org/wiki/Mandelbulb
) et la [Mandelbox](https://fr.wikipedia.org/wiki/Mandelbox) 
peuvent être considérés comme les **icônes** de l'art fractal.

Mandelbulb | Mandelbox  
![Mandelbulb](https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/
Mandelbulb_5_iterations.png/440px-Mandelbulb_5_iterations.png){width=300}
![Mandelbox](https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/
Mandelbox2.jpg/440px-Mandelbox2.jpg){width=300}


L'art fractal est rarement dessiné ou peint à la main, mais plutôt créé 
à l'aide d'[ordinateurs](https://fr.wikipedia.org/wiki/Ordinateur), 
lesquels sont en effet capables de calculer des fonctions fractales 
et d'engendrer des images à partir de ces dernières : la [récursivité](
https://fr.wikipedia.org/wiki/R%C3%A9cursivit%C3%A9
). 
C'est d'ailleurs l'apparition des ordinateurs qui a permis 
le développement de cet art, car il demande une grosse puissance de [calcul](
https://fr.wikipedia.org/wiki/FLOPS
).

> Les programmes générateurs d'images fractales fonctionnent habituellement 
en trois étapes :  
1. le réglage des paramètres qui encadrent la génération de l'image  
2. l'exécution des calculs  
3. l'application des résultats à un plan pour générer une image.  

Pour une animation, l'opération devra être répétée pour chaque image générée.  
Dans certains cas, d'autres [logiciels graphiques](
https://fr.wikipedia.org/wiki/Logiciel_graphique) sont ensuite utilisés 
pour modifier l'image produite : c'est la **postproduction**.  
Des images non fractales peuvent aussi être intégrées à l'œuvre.

>> Les fractales sont générées en utilisant la méthode [itérative](
https://fr.wikipedia.org/wiki/It%C3%A9ration) pour résoudre des équations 
non linéaires ou des [équations polynomiales](https://fr.wikipedia.org/wiki/
%C3%89quation_polynomiale).  
L'augmentation de la puissance des ordinateurs a permis la création 
de logiciels permettant le calcul d'images tridimensionnelles 
en image de synthèse proposant ainsi les fonctions et effets 
habituellement réservés aux logiciels de [modélisation tridimensionnelle](
https://fr.wikipedia.org/wiki/Mod%C3%A9lisation_tridimensionnelle) classiques 
(lumières, lumières volumétriques, flou de profondeur, atmosphère, 
réflexion/réfraction de certains matériaux, textures, ...).  

_"Lost Menger Sponge" par Marc Vanlindt || "Fractal Dead Tree" par 
Marc Vanlindt || Impression 3D "Fractal Dead Tree"_  
![vanlindt1](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f5/%22Lost_Menger_Sponge%22_par_Marc_Vanlindt.jpg/
1280px-%22Lost_Menger_Sponge%22_par_Marc_Vanlindt.jpg){width=350}
![vanlindt2](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/%22Fractal_Dead_Tree%22_par_Marc_Vanlindt.jpg/
1280px-%22Fractal_Dead_Tree%22_par_Marc_Vanlindt.jpg){width=350}
![printed3D](https://upload.wikimedia.org/wikipedia/commons/6/69/
Impression_d%27une_fractale_3D.png){width=200}

La démarche artistique pour créer une fractale en 3D est la même que pour 
une fractale en 2D. Le fait que [Kerry Mitchell](https://en.wikipedia.org/
wiki/Kerry_Mitchell) ait écrit que l'art fractal était "un sous-groupe de 
l'art visuel bidimensionnel" ne signifie donc pas que les réalisations 
faites en 3D ne sont pas de l'art fractal mais que ^^les fractales 3D 
n'existaient tout simplement pas encore en 1999 car la puissance 
des ordinateurs de l'époque ne le permettait pas^^ 
et que personne n'avait encore produit une version 
tridimensionnelle de l'ensemble de Mandelbrot. 

---

## 📚 Types

Les fractales apparentées à l'art fractal peuvent être divisées 
en différents groupes, ou catégories :

- Les fractales ==dérivées de la géométrie== :  
    Elles sont générées en utilisant des transformations itératives 
    sur une figure initiale simple, comme un segment ([la poussière de Cantor]
    (https://fr.wikipedia.org/wiki/Ensemble_de_Cantor) ou le flocon de Koch), 
    un triangle ([le triangle de Sierpiński](https://fr.wikipedia.org/wiki/
    Triangle_de_Sierpi%C5%84ski)), ou un cube ([l'éponge de Menger](
    https://fr.wikipedia.org/wiki/%C3%89ponge_de_Menger)). 
    Les premières figures fractales découvertes à la fin du XIXe siècle 
    et au début du XXe siècle appartiennent à ce groupe ;  

    _Cantor :_    
    ![cantor](
    https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/
    Cantor_set_in_seven_iterations.svg/
    1920px-Cantor_set_in_seven_iterations.svg.png){width=350}  

    _Sierpiński :_  
    ![sierpiński](
    https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/
    Sierpinski_triangle_evolution.svg/
    1920px-Sierpinski_triangle_evolution.svg.png){width=350}  

    _Menger (après 4 itération et coupée par un plan transversal :_  
    ![menger1](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/
    Menger-Schwamm.jpg/800px-Menger-Schwamm.jpg){width=500}
    ![menger1](https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/
    Menger4_Coupe.jpg/360px-Menger4_Coupe.jpg){width=400}  

- Les ==ensembles== fractals :  
    Comme l'ensemble de Mandelbrot, l'ensemble de Julia ou 
    [la fractale de Liapounov](https://fr.wikipedia.org/wiki/Fractale_de_Liapounov)   
    _Liapounov :_  
    ![liapounov](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Lyapunov-fractal-AB.png/640px-Lyapunov-fractal-AB.png){width=400}  

- les ==[systèmes de fonctions itérées](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_fonctions_it%C3%A9r%C3%A9es)== :  
_Exemple de la [fougère de Barnsley](https://fr.wikipedia.org/wiki/
Foug%C3%A8re_de_Barnsley), élaborée par un système de quatre fonctions affines_  
![](https://upload.wikimedia.org/wikipedia/commons/4/4b/
Fractal_fern_explained.png){width=300}  

- les ==[attracteurs](https://fr.wikipedia.org/wiki/Attracteur)== :  
    _Exemple de représentation visuelle d'un attracteur étrange_  
    ![attracteur](https://upload.wikimedia.org/wikipedia/commons/4/4a/Attractor_Poisson_Saturne.jpg){width=400}  

- les ==[flammes fractales](https://en.wikipedia.org/wiki/Fractal_flame)== :  
    _Créée par [Electric Sheep](https://en.wikipedia.org/wiki/Electric_Sheep) 
    | par [Apophysis](https://en.wikipedia.org/wiki/Apophysis_(software)) :_  

    ![elct](https://upload.wikimedia.org/wikipedia/commons/3/35/
    Electricsheep-14156.jpg){width=400}
    ![apo](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/
    Swirly_belt444.jpg/1024px-Swirly_belt444.jpg)    

- les fractales appartenant au ==système de Lindenmayer ou [L-Système]()== :  
    _Comme les plantes et arbres fractales_  
    ![arb](https://upload.wikimedia.org/wikipedia/commons/b/b1/Fractal_tree_%28Plate_b_-_3%29.jpg){width=400}  

- les fractales créées en itérant des ==polynômes complexes== 
(sans doute les fractales les plus spéctaculaires) :  
    ![mandel](https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Mandel_zoom_14_satellite_julia_island.jpg/
    1920px-Mandel_zoom_14_satellite_julia_island.jpg)  
    ![mandel2](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Mandel_zoom_12_satellite_spirally_wheel_with_julia_islands.jpg/1920px-Mandel_zoom_12_satellite_spirally_wheel_with_julia_islands.jpg)  

- les fractales de ==[Newton](https://fr.wikipedia.org/wiki/
Fractale_de_Newton)== :  
    _Polynômes $p(z)=z^3-1$ et $p(z)=z^5-1$ :_  
    ![newton](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/Julia-set_N_z3-1.png/800px-Julia-set_N_z3-1.png){width=500}
    ![newton2](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Newtroot_1_0_0_0_0_m1.png/800px-Newtroot_1_0_0_0_0_m1.png){width=400}  

- les fractales ==quaternioniques== et (récemment) ==hypernioniques== ;

- les ==[terrains fractales](https://fr.wikipedia.org/wiki/Terrain_fractal)== ;  

- les fractales ==3D== :  
    Mandelbulb, Mandelbox, Menger Sponge, [Cube de Jerusalem](https://fr.wikipedia.org/wiki/Cube_de_J%C3%A9rusalem), [Tricorn](https://en.wikipedia.org/wiki/Tricorn_%28mathematics%29), ...  
    _Cube de Jerusalem (itération 3) | Tricorn :_  
    ![cube](https://upload.wikimedia.org/wikipedia/commons/3/30/Cube_de_J%C3%A9rusalem%2C_it%C3%A9ration_3.png){width=350}
    ![tric](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Multibar.gif/440px-Multibar.gif)

> L'« **expressionnisme fractal** » est un terme utilisé pour différencier 
l'art fractal de l'art visuel traditionnel qui incorpore 
des éléments fractals comme des autosimilarités.  
Un exemple d'expressionnisme fractal est les motifs de gouttes 
de [Jackson Pollock](https://fr.wikipedia.org/wiki/Jackson_Pollock). 
Ils ont été analysés et ils contiennent une dimension fractale 
qu'on a attribuée à sa technique.    

![gouttes](https://media.istockphoto.com/photos/-picture-id114380408?s=612x612)  

---

## 🔑 Techniques

>>> Toutes sortes de fractales ont été utilisées comme base 
pour l'art numérique.  
Des images colorées en haute qualité graphique sont devenues de plus en plus 
accessibles dans les laboratoires de recherche scientifique 
dans les années 1980. Des **formes d'art scientifique**, comme l'art fractal, 
se sont développées séparément de la **culture dominante** 
(= le courant de pensée ou de croyance d'une majorité, 
pas nécessairement cohérent). En commençant par des images montrant 
les détails de fractales bidimensionnelles comme l'ensemble de Mandelbrot, 
les fractales ont trouvé des applications artistiques dans de nombreux domaines, 
aussi variés de la génération de **texture, la simulation de pousse 
de plante et la génération de paysages**.  

>> Les fractales sont parfois combinées avec des algorithmes évolutionnistes, 
soit en choisissant itérativement des spécimens jugés beaux dans un ensemble de 
**variation aléatoire** d'une œuvre fractale et en produisant ensemble 
de nouvelles variations, pour éviter d'obtenir des résultats incertains 
ou peu satisfaisants, ou collectivement, comme dans le projet Electric Sheep, 
où les gens utilisent des flammes fractales réalisées grâce au calcul distribué 
comme écran de veille. Ils peuvent ensuite « noter » les flammes fractales 
qu'ils voient et ces notes influencent le serveur qui adapte l'algorithme 
pour réduire les chances d'obtenir des flammes fractales jugées indésirables 
et augmenter les chances d'obtenir des flammes fractales désirables. 
**Ce projet est donc une œuvre d'art générée par ordinateur 
et créée par une communauté entière**.

> Beaucoup d'images fractales sont admirées pour l'harmonie que les gens 
y perçoivent. Ce résultat est souvent réussi grâce aux motifs émergeant de 
l'**équilibre entre ordre et chaos**. Des qualités similaires ont été décrites 
dans les [peintures chinoises](https://fr.wikipedia.org/wiki/Peinture_chinoise) 
et les [penjings](https://fr.wikipedia.org/wiki/Penjing).  


!!! example "Python"
    Les images de fractales sont réalisables grâce à des algorithmes exécutés 
    par des machines. Prenons le langage **Python** pour des exemples de codage 
    et de rendu numérique :  

    === "Flocon de Koch"
    ??? note "Entrée"  
        ```python
        from turtle import *

        def koch_n(n: int, longueur: int):
            speed(0)
            pencolor("blue")
            if n==0 :
                forward(longueur)
            else :
                koch_n(n-1, longueur/3)
                left(60)
                koch_n(n-1, longueur/3)
                right(120)
                koch_n(n-1, longueur/3)
                left(60)
                koch_n(n-1, longueur/3)
        koch(4, 300)    
        ```

    ??? done "Sortie"  
        ![codekoch](/images/gifkoch.gif)  


    === "Triangle de Sierpiński"
    ??? note "Entrée"  
        ```python
        from turtle import *
    
        def sierpinski(n:int, longueur: int):
            speed(0)
            pencolor("red")
            if n==0 :
                for i in range (0, 3):
                    forward(longueur)
                    left(120)
            if n>0:
                sierpinski(n-1, longueur/2)
                forward(longueur/2)
                sierpinski(n-1, longueur/2)
                backward(longueur/2)
                left(60)
                forward(longueur/2)
                right(60)
                sierpinski(n-1, longueur/2)
                left(60)
                backward(longueur/2)
                right(60)
        sierpinski(4,600)
        ```  

    ??? done "Sortie"  
        ![codesierp](/images/giftriangle.gif)  


    === "Spirale de Fibonacci"  
    ??? note "Entrée"  
        ```python
        import turtle
        import math
 
        def fiboPlot(n):
            a = 0
            b = 1
            square_a = a
            square_b = b
 
            # Setting the colour of the plotting pen to blue
            x.pencolor("blue")
 
            # Drawing the first square
            x.forward(b * factor)
            x.left(90)
            x.forward(b * factor)
            x.left(90)
            x.forward(b * factor)
            x.left(90)
            x.forward(b * factor)
 
            # Proceeding in the Fibonacci Series
            temp = square_b
            square_b = square_b + square_a
            square_a = temp
     
            # Drawing the rest of the squares
            for i in range(1, n):
                x.backward(square_a * factor)
                x.right(90)
                x.forward(square_b * factor)
                x.left(90)
                x.forward(square_b * factor)
                x.left(90)
                x.forward(square_b * factor)
 
                # Proceeding in the Fibonacci Series
                temp = square_b
                square_b = square_b + square_a
                square_a = temp
 
            # Bringing the pen to starting point of the spiral plot
            x.penup()
            x.setposition(factor, 0)
            x.seth(0)
            x.pendown()
 
            # Setting the colour of the plotting pen to red
            x.pencolor("red")
 
            # Fibonacci Spiral Plot
            x.left(90)
            for i in range(n):
                print(b)
                fdwd = math.pi * b * factor / 2
                fdwd /= 90
                for j in range(90):
                    x.forward(fdwd)
                    x.left(1)
                temp = a
                a = b
                b = temp + b
                    # Here 'factor' signifies the multiplicative
        # factor which expands or shrinks the scale
        # of the plot by a certain factor.
        factor = 1
 
        # Taking Input for the number of
        # Iterations our Algorithm will run
        n = int(input('Enter the number of iterations (must be > 1): '))
 
        # Plotting the Fibonacci Spiral Fractal
        # and printing the corresponding Fibonacci Number
        if n > 0:
            print("Fibonacci series for", n, "elements :")
            x = turtle.Turtle()
            x.speed(100)
            fiboPlot(n)
            turtle.done()
        else:
            print("Number of iterations must be > 0")
        ```  

    ??? done "Sortie"  
        ![codefibo](/images/giffibo.gif)  
 

Bonus : _Fractale du mot Fibonacci :_
![motfibo](https://upload.wikimedia.org/wikipedia/commons/9/99/
FWF_Samuel_Monnier_d%C3%A9tail.jpg)
 
---

## 💻 Principaux logiciels

De nombreux programmes existent pour générer des fractales 2D ou 3D, 
cela étant la seule tâche que ces logiciels réalisent. 
Les principaux sont **Fractint, Sterling ou XaoS**.  

> De nombreux autres logiciels permettent de réaliser des fractales 
bien que ce ne soit pas le but premier.  
> Nous retrouvons dans cette catégories tous les logiciels proposant 
de passer par un langage de programmation pour réaliser son objet.

Les principaux logiciels proposant cette possibilité sont :  
- [Autocad](https://fr.wikipedia.org/wiki/AutoCAD) : 
proposant l'utilisation du langage LISP  
- [Blender](https://fr.wikipedia.org/wiki/Blender) : 
proposant l'utilisation du Python  
- [FreeCAD](https://fr.wikipedia.org/wiki/FreeCAD) : 
proposant l'utilisation du Python  
- [OpenSCAD](https://fr.wikipedia.org/wiki/OpenSCAD): 
ayant son propre langage, sans nom officiel actuellement.  

>La plupart des logiciels vectoriels ([Inkscape](https://fr.wikipedia.org/
wiki/Inkscape), [Illustrator](https://fr.wikipedia.org/wiki/Adobe_Illustrator), 
[CorelDraw](https://fr.wikipedia.org/wiki/CorelDRAW), ...) 
proposent également maintenant des outils de fractalisation 
(Koch, système de Lindemayer, fractalisation de vecteur, ...) 

---

## 🌄 Paysages

La première image fractale destinée à être une œuvre d'art fut probablement 
la couverture du **Scientific American en août 1985**.  
![magazine1985](images/magazine_scienc.jpg)  

Cette image montrait un paysage formé de la fonction potentielle du domaine 
hors de l'ensemble de Mandelbrot habituel. Néanmoins, 
comme la fonction potentielle croît rapidement près de la limite 
de l'ensemble de Mandelbrot, il fut nécessaire pour le créateur de laisser 
le paysage grandir vers le bas, pour qu'il semble que l'ensemble 
est un plateau au sommet d'une montagne avec des flancs abrupts.  
La même technique fut utilisée un an plus tard dans [The Beauty of Fractals 
de Heinz-Otto Peitgen et Peter Richter](https://upload.wikimedia.org/
wikipedia/en/d/df/BeautyOfFractalsBook.jpg). Ils fournirent une formule 
pour estimer la distance d'un point hors de l'ensemble de Mandelbrot 
à la limite de l'ensemble de Mandelbrot (et une formule similaire 
pour l'ensemble de Julia).  
Des paysages peuvent, par exemple être formés par la fonction distance 
d'une famille d'itérations de la forme $z^2 + az^4 + c$. 

!!! example "Exemples d'objets fractals dans la nature"

    === "Le chou romanesco"
        ![chou](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Romanesco_broccoli_%28Brassica_oleracea%29.jpg/
        1920px-Romanesco_broccoli_%28Brassica_oleracea%29.jpg){width=400}

    === "Le flocon de neige"
        ![neige](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/
        Snowflake_macro_photography_1.jpg/
        1920px-Snowflake_macro_photography_1.jpg){width=400}

    === "La fougère"
        ![foug](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/
        Dryopteris.filix-mas.jpg/542px-Dryopteris.filix-mas.jpg){width=400}

    === "La coquille d'escargot et les coquillages en général"
        ![escarg](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/
        Zonitoides_nitidus_drawing.svg/
        800px-Zonitoides_nitidus_drawing.svg.png){width=200}

    === "Les feuilles"
        ![érable](https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/
        Maple_leaves.jpg/440px-Maple_leaves.jpg){width=400}
        ![noyer](https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/
        Eschenblattjung.jpg/1920px-Eschenblattjung.jpg){width=400}

    === "Les tentatucules"
        ![tenta](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Img_octopus_arm_and_suckers_057513.jpg/
        1024px-Img_octopus_arm_and_suckers_057513.jpg){width=200}

    === "L'iris"
        ![iris](https://upload.wikimedia.org/wikipedia/commons/e/ee/
        Iris_oeil_1.png){width=200}

!!! example "Exemples de paysage avec des caractéristiques de fractales"  

    === "Le Lac Nasser en Égypte"
        ![egy](images/egypte.jpg) 

    === "Le Grand Canyon en Arizona"
        ![canyon](images/canyon.jpg)

    === "Novaya Zemlya en Russie"
        ![rus](images/russie.jpg)

    === "Le plateau tibétain et une partie de la chaîne de montagnes 
    de l'Himalaya"
        ![himalaya](images/tibétain.jpg)

    === "Parc National de Doñana en Andalousie en Espagne"
        ![andalous](images/esp.jpg){width=300}
        ![andal](images/esp1.jpg){width=300}
        ![and](images/esp2.jpg){width=300}

---

## 🎨 Artistes

| Nom                                                                                  | Visage                                                                                                                                   | Qui est-ce ?                                                                                                                                                                                    | Une oeuvre parmi d'autres                                                                                                                               | A savoir                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
|--------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [William Latham](https://fr.wikipedia.org/wiki/William_Latham) (1961)                | ![tetewl](https://tse3.mm.bing.net/th?id=OIP.93cn6RwruGbnW0QGs-s3MQHaJ4&pid=Api)                                                         | Artiste et chercheur britannique associant sensibilité artistique et démarche scientifique                                                                                                      | _ZapQ3 on the Plane of Infinity. Ray Traced (1991)_ ![oeuvwl](images/plane-infinity.jpg)                    | Il a utilisé la géométrie fractale et d'autres graphiques générés par ordinateur dans ses travaux. Connu pour son art organique pionnier créé à la fin des années 80 et au début des années 90 alors qu'il était chercheur à IBM à Winchester                                                                                                                                                                                                                                                          |
| [Greg Sams](nhttps://en.wikipedia.org/wiki/Greg_Sams) (1948)                         | ![tetegs](https://upload.wikimedia.org/wikipedia/commons/f/fb/Gregory_%28Greg%29_Sams%2C_Glade_Festival%2C2006.jpg)                      | Artiste fractal, auteur et éditeur né aux USA mais ayant vécu en Angleterre                                                                                                                     | ![oeuvgs](images/greg.jpg)                         | Il est connu pour avoir utilisé des images fractales dans des cartes postales, des T-shirts et des textiles. Il est un utilisateur de fauteuil roulant depuis qu'il est tombé d'un arbre alors qu'il était étudiant de première année à l'Université de Californie à Berkeley. Il a déménagé à Londres âgé de 19 ans.                                                                                                                                                                                  |
| [Vicky Brago-Mitchell](https://en.wikipedia.org/wiki/Vicky_Brago-Mitchell) (1946)    | ![tetevicky](https://upload.wikimedia.org/wikipedia/commons/7/71/Vicky-wiki.jpg)                                                         | Artiste américaine de fractale connue dans les années 60 comme étudiante à l'Université de Stanford                                                                                             | _Eternal 3 (2009)_ ![oeuvm](images/eternal.jpg)                               | Elle a créé des œuvres d'art fractal qui sont apparues dans des expositions et sur des couvertures de magazines.                                                                                                                                                                                                                                                                                                                                                                                       |
| [Scott Draves](https://en.wikipedia.org/wiki/Scott_Draves) (1968)                    | ![tetesd](https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Scott_Draves_2014.png/500px-Scott_Draves_2014.png)                   | L'inventeur des flammes fractales, leader du projet de calcul distribué Electric Sheep, video artiste américain et [VJing accompli](https://en.wikipedia.org/wiki/VJing)                        | Image venant de Electric Sheep ![oeuvsd](https://upload.wikimedia.org/wikipedia/commons/8/89/Electricsheep-0-1000.jpg)                                  | Son travail a été exposé au bureau de Google à New York en été 2010                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| [Carlos Ginzburg](https://fr.wikipedia.org/wiki/Carlos_Ginzburg) (1946)              | ![cg](images/carlos.jpg)                       | Artiste conceptuel et théoricien de l'Art fractaliste. Né en Argentine et ayant vécu en France                                                                                                  | _Récursivité-La Subjectivité fractale (2005)_ ![oeuvrecg](images/subjectivité.jpg) | Il a exploré l'art fractal et développé un concept appelé "homo fractalus" qui est basé sur l'idée que l'humain est la fractale ultime. Il a étudié la philosophie et les sciences sociales. Adhérent des premières heures du Mouvement "World Art"                                                                                                                                                                                                                                                    |
| [Jean-Claude Meynard](https://fr.wikipedia.org/wiki/Jean-Claude_Meynard) (1951-2019) | ![jc](https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Jean-Claude_Meynard%2C_2013.jpeg/520px-Jean-Claude_Meynard%2C_2013.jpeg) | Artiste français qui utilise la géométrie fractale pour explorer la complexité du réel, ses univers proliférants et labyrinthiques, à travers des œuvres picturales, sculpturales et numériques | _L'homme qui marche (2010)_ ![oeuvrejcm](images/marche.jpg)                                          | Il est signataire avec Carlos Ginzburg du Manifeste fractaliste publié par la revue Art Press en 1997. Il crée aussi, selon le processus de réplication fractale, des bouleversements architecturaux nommés: "Demeures Fractales". Éphémères par destination, elles ont fait l'objet de nombreux films dont un majeur : "L'Escalier Fractal - un Manifeste Fractal". Dans son dernier livre, il propose de reconfigurer, avec la géométrie fractale comme outil de connaissance, notre vision du monde |  


L'artiste amériacin [Kerry Mitchell](https://en.wikipedia.org/wiki/
Kerry_Mitchell) a écrit un Manifeste de l'art fractal, déclarant que 
> « L'art fractal est un **sous-groupe de l'art visuel bidimensionnel** 
et est similaire en beaucoup d'aspects à la photographie, 
une autre forme d'art qui a été accueillie avec beaucoup de scepticisme 
à ses débuts. Les images fractales sont la plupart du temps imprimées, 
ce qui amène l'artiste fractal au même niveau que les **peintres**, 
les **photographes** et les **graveurs**. Les fractales existent nativement 
en tant qu'images électroniques. C'est un format que les artistes 
visuels traditionnels embrassent de plus en plus, 
ce qui les amène vers le monde digital de l'art fractal. Générer des fractales 
peut aussi être une tentative artistique, une recherche mathématique, 
ou juste une distraction. Néanmoins, l'art fractal est clairement différent 
des autres activités digitales de par ce qu'il est, 
et de par ce qu'il n'est pas. »  

Selon Mitchell, l'art fractal n'est pas un art seulement automatisé 
par ordinateur, qui manque de règle, imprévisible ou quelque chose que 
n'importe accédant à un ordinateur peut faire bien. Mais il considère 
**l'art fractal comme expressif, créatif, et demandant une entrée, 
un effort et une intelligence**.  

> Mais plus important : « l'art fractal est simplement ce qui est créé 
par les artistes fractals : de l'ART. »

En 1982, la société [Industrial Light & Magic](https://fr.wikipedia.org/wiki/
Industrial_Light_%26_Magic) (société fondée par réalisateur, scénariste 
et producteur américain George Lucas) a utilisé pour la première fois 
des fractales générées par ordinateur dans un film :  
"**Star Trek 2** : La Colère de Khan" afin de créer différents décors du film.