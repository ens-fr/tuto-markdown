# Sagas de films

De nos jours, le mot saga est repris dans le langage courant pour désigner un cycle romanesque en plusieurs volets ou certaines œuvres à caractère épique, même si elles sont exprimées sur un support autre que le support littéraire. De manière plus générale, le terme est également utilisé de manière métaphorique pour désigner une histoire — fictive ou non — qui connaîtrait de nombreux épisodes ou rebondissements.

## Des Sagas myhtiques à connaitre

### Harry Potter

[Harry Potter](https://fr.wikipedia.org/wiki/Harry_Potter#Cin%C3%A9ma) est une série littéraire de low fantasy écrite par l'auteure britannique J. K. Rowling, dont la suite romanesque s'est achevée en 2007. La série de sept romans raconte les aventures d'un jeune sorcier nommé Harry Potter et de ses amis Ron Weasley et Hermione Granger à l'école de sorcellerie Poudlard, dirigée par Albus Dumbledore. L'intrigue principale de la série met en scène le combat de Harry contre Lord Voldemort, un mage noir à la recherche de l'immortalité ayant autrefois assassiné les parents du garçon. À la tête de ses fidèles adeptes, _les Mangemorts_, Voldemort cherche depuis des décennies à acquérir le pouvoir absolu sur le monde des sorciers et des _Moldus_ (les humains sans pouvoirs magiques). 

Huit films à succès rapportent au total plus de 8 milliards de dollars et accèdent à la troisième place des franchises les plus rentables de tous les temps après celles de l'univers Marvel et de Star Wars.



=== "À l'école des sorciers"

    Peu avant son onzième anniversaire, Harry reçoit une lettre l'invitant à se présenter lors de la rentrée des classes à l'école de sorcellerie de Poudlard. Harry va découvrir le monde des sorciers et apprendre à maîtriser et utiliser les pouvoirs magiques qu'il possède et se fait deux amis inséparables : Ronald Weasley et Hermione Granger. 

=== "La Chambre des Secrets"

    L'année suivante, Harry et ses amis doivent faire face à une nouvelle menace à Poudlard. La fameuse Chambre des secrets, bâtie plusieurs siècles plus tôt par l'un des fondateurs de l'école, Salazar Serpentard, aurait été rouverte par son « héritier ». Selon la légende, contiendrait un gigantesque monstre destiné à tuer les enfants sorciers nés de parents moldus acceptés à l'école contre le souhait de Serpentard. 
      
=== "Le Prisonier d'Askaban"

    À l'été 1993, les sorciers, ainsi que les Moldus, sont informés de l'évasion de prison d'un dangereux criminel nommé Sirius Black. Harry apprend que la motivation de Black est de le tuer afin de permettre à Voldemort, son maître, de retrouver l'étendue de son pouvoir. En fin d'année, Sirius Black parvient à attirer Harry, Ron et Hermione à l'extérieur de l'école et, en présence de Lupin qui vient les retrouver, leur explique les réelles motivations de son évasion : retrouver et tuer Peter Pettigrow.

=== "La Coupe de Feu"

    Une édition du célèbre tournoi des Trois Sorciers se déroule exceptionnellement à Poudlard et deux autres délégations européennes se rendent sur place pour participer à la compétition : des élèves de l'Académie de magie Beauxbâtons et ceux de l'Institut Durmstrang. La Coupe de feu, juge impartiale chargée de sélectionner le champion de chaque école, choisit exceptionnellement deux champions pour Poudlard : Cedric Diggory et Harry Potter, ce dernier n'ayant pourtant pas l'âge requis pour participer à la compétition. Harry se voit contraint de participer au tournoi, qui se déroule sur trois épreuves réparties sur l’année. 

=== "L'Ordre du Phénix"

    Harry retrouve son parrain Sirius, Lupin, Hermione et la famille Weasley au 12 square Grimmaurd, qui devient le quartier général de l'ordre du Phénix, l'organisation fondée par Dumbledore au moment de la première ascension de Voldemort. 
    
    Le ministère de la Magie de son côté, malgré les événements de l’an passé, refuse d'admettre le retour du mage noir. Harry, Ron et Hermione retournent à Poudlard, où un nouveau professeur de défense contre les forces du mal, Dolores Ombrage, engagée par le ministre de la Magie lui-même. Hermione décide d'agir et de fonder une seconde organisation au sein-même de l'école, l'Armée de Dumbledore, pour contrer Ombrage et inciter les élèves volontaires à pratiquer la magie pour apprendre à se défendre face aux dangers extérieurs que les autorités souhaitent taire.

=== "Le Prince de Sang-mêlé" 

    Ce sixième film se concentre davantage sur l'histoire de Voldemort. Un passé que Harry et Dumbledore éclaircissent en visionnant les souvenirs des personnes ayant fréquenté le mage noir durant sa jeunesse. Ils apprennent ainsi l'existence des horcruxes, des fragments d'âmes de Voldemort que celui-ci aurait réparti en différents objets, et dont leur simple existence le rendrait immortel. 

    Harry récupère un vieux manuel ayant appartenu à un certain « Prince de sang-mêlé ». Le livre regorge d'une multitude de conseils et de notes ajoutés à la main par son ancien propriétaire.

=== "Les Reliques de la Mort 1 & 2"

    Harry, Ron et Hermione, âgés à présent de 17 ans, décident de ne pas retourner à Poudlard et de se consacrer entièrement à la recherche des horcruxes. Ils trouvent le médaillon de Serpentard au ministère de la Magie et apprennent que l'épée de Gryffondor a permis à Dumbledore de briser la bague horcruxe des Gaunt l'année précédente.

    Le trio apprend l'existence de trois reliques très puissantes : la baguette de sureau, la pierre de Résurrection et la cape d'invisibilité , faisant du sorcier qui les possède un « Maître de la mort ». 


### Star Wars

[Star Wars](https://fr.wikipedia.org/wiki/Star_Wars#Films) (à l'origine nommée sous son titre français, La Guerre des étoiles) est un univers de science fantasy créé par George Lucas. D'abord conçue comme une trilogie cinématographique sortie entre 1977 et 1983, la saga s'accroît ensuite, entre 1999 et 2005, de trois nouveaux films, qui racontent des événements antérieurs à la première trilogie. 

Les droits d'auteur de Star Wars sont achetés en 2012 par la Walt Disney Company pour 4,05 milliards de dollars : la sortie au cinéma du septième épisode de la saga et premier de la troisième trilogie est alors planifiée pour 2015.

=== "Trilogie originale"

    * Épisode IV

    Dix-neuf années se sont écoulées depuis La Revanche des Sith. Mais l’Empire Galactique dirigé par Palpatine et son homme de main Dark Vador doit aujourd’hui faire face aux Rebelles. L’une d’entre elles, la Princesse Leia, est capturée et missionne deux droïdes de retrouver un vieil ermite. Sur leurs routes, ils font la connaissance d’un ouvrier agricole, un certain Luke Skywalker, qui se révèle être un puissant détenteur de la Force Jedi. Rejoints par un duo de contrebandiers, ils vont tenter de libérer la Princesse et de détruire l’arme ultime de l’Empire : l’Étoile noire.

    * Épisode V

    Malgré la destruction de l’Étoile noire dans Un nouvel espoir, l’Empire galactique est toujours aussi puissant et continue à persécuter les rebelles. Ceux-ci ont élu domicile sur la planète des glaces, Hoth. Le jeune Luke Skywalker s’en va quant à lui trouver un nouveau maître Jedi afin de maîtriser la Force. De leur côté, Han Solo et Leia s’en vont trouver de l’aider dans une étrange cité perchée dans les nuages et retrouvent une vielle connaissance de Han.

    * Épisode VI

    Sur Tatooine, les rebelles tentent de libérer Han Solo, qui s’est fait cryogéniser et a échoué entre les mains du criminel Jabba Le Hutt. Après une rude bataille, Luke Skywalker part faire ses adieux à son maître Yoda, tandis que toute l’équipe de héros se retrouve sur la planète Endor, afin de tenter de faire exploser la nouvelle Étoile de la mort. Luke s’en va finalement combattre l’Empereur et tente de rallier à sa cause Dark Vador, qui n’est autre que son propre père.

=== "Prélogie"

    * Épisode I 

    Il y a bien longtemps, dans une galaxie lointaine, très lointaine… Qui-Gon Jinn et Obi-Wan Kenobi, deux chevaliers Jedi, sont dépêchés afin de régler un conflit sur la petite planète Naboo, mais se retrouvent pris en embuscade. Les événements vont rapidement les précipiter dans un conflit de dimension galactique et les entraîner sur une autre planète où ils feront la connaissance d’un jeune esclave, Anakin Skylwalker, qui se révèlera un puissant détenteur de la Force Jedi.

    * Épisode II

    Dix ans après les événements de La Menace Fantôme, Anakin est désormais un jeune apprenti Jedi (padawan) aux côtés de son maître, Obi Wan Kenobi. Jeune et impétueux, il est cependant tiraillé de toute part entre son amitié pour le chancelier Palpatine, son amour pour la désormais sénatrice Amidala et sa fidélité au conseil Jedi. Parallèlement, Obi-Wan va découvrir un complot visant à faire rentrer la République en guerre contre la Fédération du Commerce à l’aide d’une armée de clones commandée il y a des années par un maître Jedi, aujourd’hui disparu.

    * Épisode III

    Trois ans après l’Attaque des clones, la galaxie est en guerre. Obi-Wan et Anakin sont chargés de libérer le chancelier Palpatine, qui vient d’être enlevé par le général Grievous et le comte Dooku. En réalité, Palpatine est le véritable ennemi, puisque c’est lui le seigneur Sith que les Jedi tentent de démasquer depuis le début. Sous son influence, Anakin cède de plus en plus au côté obscur de la Force : il se détache de son maître Obi-Wan et de sa femme, la reine Amidala, quitte à franchir la ligne et à devenir le nouveau seigneur Sith.

=== "Troisième trilogie"

    * Épisode VII

    Trente années ont passé depuis la destruction de l’Étoile noire, et la mort de Dark Vador et du Chancelier Suprême Palpatine. Luke Skywalker et tous les autres Jedi ont mystérieusement disparu, tandis qu’un nouvel ordre s’est peu à peu substitué à l’Empire galactique. Baptisé le Premier Ordre, il compte dans ses rangs le Sith Kylo Ren, qui fait régner la terreur dans toute la galaxie et qui répond aux ordres du mystérieux Snoke. C’est dans ce contexte que la jeune Rey recueille un jour le droid BB-8, lequel détient des indices sur la localisation de Luke. Aux commandes du Faucon Millenium, qu’elle retrouve un peu par hasard, la voilà plongée au coeur d’un complot qui semble totalement la dépasser…

    * Épisode VIII

    Alors que Rey a finalement réussi à retrouver Luke Skywalker sur la planète Ahch-To, la Résistance et le Premier Ordre continuent à se livrer bataille dans l’espace. Le général Hux parvient à prendre en embuscade les vaisseaux de la Résistance et met en péril toute la fragile organisation qui lutte contre le terrible suprême leader Snoke. Rey tente de convaincre Luke de l’aider et de l’entraîner à maîtriser la Force. Mais dans le même temps, un étrange lien commence à se nouer entre la jeune femme et son plus terrible ennemi : Kylo Ren.

    * Épisode IX

    Un an a passé depuis que Kylo Ren a tué Snoke, le Leader suprême et pris sa place. Bien que largement décimée, la Résistance est prête à renaître de ses cendres. Rey, Poe, Leia et leurs alliés se préparent à reprendre le combat. Mais ils vont devoir faire face à un vieil ennemi : l'empereur Palpatine...

## Les majors du cinéma

Dans le monde du cinéma, les majors sont les plus gros studios de production américains. Leur nombre a varié selon les périodes et surtout leurs actionnaires. Considérées comme indépendantes durant l'Âge d'Or des années 1920-1930, les sociétés de production ont par la suite changé de propriétaires, de statuts, disparu ou fusionné. 

^^Les 5 plus grands :^^

1. [RKO Pictures](https://fr.wikipedia.org/wiki/RKO_Pictures)
2. [20th Century Fox Film Corporation](https://fr.wikipedia.org/wiki/20th_Century_Studios)
3. [Warner Bros. Pictures](https://fr.wikipedia.org/wiki/Warner_Bros.)
4. [Paramount Pictures](https://fr.wikipedia.org/wiki/Paramount_Pictures)
5. [Metro-Goldwyn-Mayer](https://fr.wikipedia.org/wiki/Metro-Goldwyn-Mayer)

<iframe width="420" height="320" src="https://www.youtube.com/embed/jmF0edcnw4w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>