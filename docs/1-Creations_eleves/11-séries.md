# Série Télévisée


Une **série télévisée**, abrégé en **série**,est une œuvre télévisuelle qui se déroule en plusieurs parties d'une durée généralement équivalente, appelées « _épisodes_ ». Le lien entre les épisodes peut être l’histoire, les personnages ou le thème de la série. 

## Historique

Bien que de nombreux pays aient produit des séries, dont certaines de grande qualité, l'histoire des séries télévisées a été dominée principalement par trois pays : les États-Unis, la France et le Royaume-Uni. Généalogiquement, la série télévisée vient d'une part du serial, format inventé peu avant la Première Guerre mondiale et qui est un film découpé en épisodes et projeté en salle, et, d'autre part, de la dramatique radiophonique. 

## Genre

* Comique 

Le genre comique correspond généralement aux sitcoms mais il existe également des comédies dramatiques, adoptant le format des séries dramatiques, comme [Ally McBeal](https://fr.wikipedia.org/wiki/Ally_McBeal) ou [Desperate Housewives](https://fr.wikipedia.org/wiki/Desperate_Housewives).

* Dramatique

Le genre dramatique (_drama en anglais_) correspond à la plupart des séries réalistes dont les séries policières, judiciaires, médicales ou politiques. 

* Action/aventure

Le genre action/aventure est synonyme de poursuites, d'affrontements et de voyages. 

* Fantastique et science-fiction

Le fantastique et la science-fiction sont souvent rattachés en un seul genre. Fredric Brown distingue cependant le fantastique de la science-fiction ainsi : « _Le fantastique traite de choses qui ne sont pas et ne peuvent pas être. La science-fiction traite de choses qui ne sont pas mais qui pourront être un jour_ ». 

* Série historique

En Europe, la série historique est l’équivalent aux États-Unis du western. 

* Animation

Les séries télévisées d'animation (ou dessins animés) sont le plus souvent destinées aux enfants, comme [Bob l'éponge](https://fr.wikipedia.org/wiki/Bob_l%27%C3%A9ponge), mais certaines ciblent un public plus large, comme [Les Simpson](https://fr.wikipedia.org/wiki/Les_Simpson) ; d'autres encore sont même déconseillées aux enfants ([South Park](https://fr.wikipedia.org/wiki/South_Park)). 

* Jeunesse

Les séries télévisées pour la jeunesse peuvent aborder tous les genres existants (l'animation étant la plus exploitée) tout en étant adaptées à l'âge auquel elles sont destinées. Le terme « jeunesse » est généralement divisé en trois catégories : la petite enfance, l'enfance et l'adolescence (jusqu'à la majorité). Par exemple, [Les Télétubbies](https://fr.wikipedia.org/wiki/Les_T%C3%A9l%C3%A9tubbies) s'adresse à la petite enfance ; [Les Mystérieuses Cités d'or](https://fr.wikipedia.org/wiki/Les_Myst%C3%A9rieuses_Cit%C3%A9s_d%27or_(s%C3%A9rie_t%C3%A9l%C3%A9vis%C3%A9e_d%27animation,_1982)) à l'enfance, et [Beverly Hills 90210](https://fr.wikipedia.org/wiki/Beverly_Hills_90210), aux adolescents. Cependant, les frontières entre les subdivisions ne sont pas fixes. 

## Évolution du mode de diffusion
  
Au cours des années, la diffusion des séries a dépassé le cadre de la télévision, avec les cassettes VHS, puis le DVD au début des années 2000, pour arriver à la diffusion Internet vers 2005 chez, entre autres, Canalplay et TF1. Cette diffusion peut se faire par téléchargement ou streaming.

## Classement des 3 série les plus regardées

1. F <span style="color: #f20;">.</span> R <span style="color: #f5;">.</span> I <span style="color: #f98;">.</span> E <span style="color: #f20;">.</span> N <span style="color: #f90;">.</span> D <span style="color: #f5;">.</span> S 

    [Friends](https://fr.wikipedia.org/wiki/Friends) est une sitcom américaine en 10 saisons, avec 236 épisodes de 22 minutes, créée par Marta Kauffman et David Crane, et diffusée entre le 22 septembre 1994 et le 6 mai 2004 sur le réseau NBC. La série raconte la vie quotidienne de ces six amis, ainsi que l'évolution de leur vie professionnelle et affective pendant dix ans. 


    ![les 6 acteurs principaux avec le logo série](https://www.sortiraparis.com/images/1001/66131/643118-serie-friends-l-episode-retrouvailles-avec-justin-bieber-et-lady-gaga-diffuse-le.jpg)


2. Breakind Bad 

    [Breaking Bad](https://fr.wikipedia.org/wiki/Breaking_Bad) est une série télévisée américaine en 62 épisodes de 47 minutes, créée par Vince Gilligan, diffusée simultanément du 20 janvier 2008 au 29 septembre 2013 sur AMC aux États-Unis et au Canada, et ensuite sur Netflix. Walter White est professeur de chimie dans un lycée, et vit avec son fils handicapé moteur et sa femme enceinte à Albuquerque, au Nouveau-Mexique. Le lendemain de son cinquantième anniversaire, on lui diagnostique un cancer du poumon en phase terminale avec une espérance de vie estimée à deux ans. Tout s'effondre pour lui ! Il décide alors de mettre en place un laboratoire et un trafic de méthamphétamine pour assurer un avenir financier confortable à sa famille après sa mort, en s'associant à Jesse Pinkman, un de ses anciens élèves devenu petit trafiquant. 


    ![deux personnages en combinaisons + logo](https://www.bilim.org/wp-content/uploads/breaking-bad-suca-sosyolojik-bakis.jpg)


3. X-Files

    [X-Files](https://fr.wikipedia.org/wiki/X-Files_:_Aux_fronti%C3%A8res_du_r%C3%A9el) : Aux frontières du réel, est une série télévisée américaine de science-fiction en 218 épisodes de 43 minutes, fondée par Chris Carter et diffusée entre le 10 septembre 1993 et le 21 mars 2018 sur le réseau Fox. Cette série oppose en permanence le normal et le paranormal, le possible et l'impossible, le réel et le surnaturel, grâce au scepticisme de Scully et à l'irrationalisme de Mulder. 


    ![une femme et un homme devant une lumière + logo avec x entouré](https://images-na.ssl-images-amazon.com/images/I/71OBgK%2BA8IL._AC_SX466_.jpg)


Cliquez [ici](https://hitek.fr/actualite/meilleurs-series-classement_7142) pour voir la suite du classement.

## Durée de vie d'une série

La durée de vie d'une série dépend tout d'abord des audiences, plus l'audience est grande plus la série continue. Des séries peuvent toutefois rester longtemps à l'antenne sans même avoir d'excellentes audiences.  Plus les séries sont vues plus il y aura de saisons, la chaîne de TV ou encore Netflix commandera une suite. . Mais si l’audience est mauvaise ou qu'il y a un manque de budget, la série peut être annulée par la production : la chaîne de TV, Netflix ou encore Amazon Prime. 

D'autres raisons peuvent expliquer le prolongement d'une série, et donc sa relative grande durée : son succès en syndication, la perspective justement de proposer une série sur ce marché, mais qui nécessite que la série atteigne au moins une centaine d'épisodes, la médiocrité ou la faiblesse des nouveaux projets proposés au cours d'une année pour la rentrée suivante, le succès d'une série sur le marché international, ou encore une base de téléspectateurs et des audiences stables pour celle-ci. 

## Producteur et diffuseur de série aux États-Unis

|  | The Walt Disney Company  | WarnerMedia | NBCUniversal | ViacomCBS | Netflix | Amazon | Apple |
|--|-------------|---|-----|-----|-----|-----|----|
| **Grand Network** | ABC | The CW | NBC | CBS, The CW |    |   |  |
| **Chaîne du câble** | FX  | HBO, TNT, Adult Swim, True TV | USA Network, Syfy, | Showtime |  |   |   |
| **SVOD**  | Disney+, Hulu  | HBO Max, Hulu | Peacock | Paramount+ | Netflix | Amazon Prime Video | Apple TV+ |
| **Major du cinéma** | Walt Disney Studio Entertainement | Warner Bros Entertainment | Universal Studio | Paramount Picture |   |   |   |



