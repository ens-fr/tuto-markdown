# La Seconde Guerre Mondiale

=== "Français"

    Vaste sujet n'est-ce-pas ? Surtout durant la troisième au collège qui était une plaie mais ne vous inquiétez plus vous êtes tombés sur la bonne page !
    Ceci est un résumé de la seconde guerre mondiale année par année accompagnée de vidéo de
    " l'histoire nous le dira"
    [Source](https://www.youtube.com/channel/UCN4TCCaX-gqBNkrUqXdgGRA), licence _Creative Commons_
    
    ## 1939- Seconde guerre mondiale tome 1
    
    En bref  
    1er avril : fin de la guerre civile en Espagne. Début de la dictature franquiste.  
    22 mai : pacte d’Acier.  
    23 août : pacte germano-soviétique.  
    1er septembre : l’Allemagne envahit la Pologne. Début de la Seconde Guerre mondiale.  
    3 septembre: LeRoyaume-Uni et La France déclarent la guerre à l'Allemagne.  
    30 novembre: début de la guerre russo-finlandaise.  
    
    [Résumé en vidéo de l'année 1939](https://www.youtube.com/watch?v=Dpezz90m7gc){ .md-button }
    
    ## 1940
    
    En bref  
    •	7 avril : campagne de Norvège.  
    •	10-13 mai : percée de Sedan.  
    •	10-28 mai : invasion de la Belgique par les troupes allemandes.  
    •	Appel du 18 juin.  
    •	Armistice du 22 juin 1940.  
    •	3 juillet : bataille de Mers el-Kébir.  
    •	5 août : début de la bataille d’Angleterre.  
    •	dans la nuit du 24 au 25 août : premier bombardement allemand sur la ville de Londres.  
    •	28 octobre : début de la guerre italo-grecque.  
    
    <iframe width="560" height="315" src="https://www.youtube.com/embed/REqenVnaHWY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    
    
    ## 1941
    En bref  
    •	22 juin : opération Barbarossa.  
    •	14 août : Charte de l’Atlantique.  
    •	7 décembre : attaque de Pearl Harbor. Début de la guerre du Pacifique. Singapour, la Malaisie, les Philippines, le Sarawak et le Nord-Bornéo sont envahis par les Japonais, qui avancent jusqu’en Birmanie à l’ouest, en Nouvelle-Guinée au sud (1941-1942).  
    
    <iframe width="560" height="315" src="https://www.youtube.com/embed/jPSC5pX4kqM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    
    ## 1942
    
    En bref  
    •	26 mai - 11 juin : bataille de Bir Hakeim.  
    •	3 - 7 juin : bataille de Midway.  
    •	16-17 juillet : rafle du Vélodrome d’Hiver.  
    •	7 août : début de la bataille de Guadalcanal.  
    •	21 août : début de la bataille de Stalingrad.  
    •	23 octobre - 3 novembre : bataille l’El Alamein.  
    •	8 novembre : opération Torch.  
    
    !!! savoir "Vidéo"
        [Résumé en vidéo de l'année 1942](https://www.youtube.com/watch?v=fLBeDzSmPUk){ .md-button }  
    
    ## 1943
    
    En bref  
    •	14 janvier :conférence de Casablanca.  
    •	2 février : victoire soviétique à la bataille de Stalingrad.  
    •	18 février : discours du Sportpalast par Joseph Goebbels.  
    •	12 - 27 mai : conférence Trident.  
    •	27 mai : création du CNR (Conseil national de la Résistance)  
    •	5 juillet - 23 août : bataille de Koursk.  
    •	7 août - 2 octobre: bataille de Smolensk.  
    •	17 août : conférence de Québec.  
    •	2 - 9 septembre : début de la campagne d’Italie.  
    •	18 octobre - 11 novembre : troisième conférence de Moscou. Le 30 octobre la déclaration de Moscou pose les bases de l’ONU.  
    •	22 novembre : conférence du Caire.  
    •	28 novembre : conférence de Téhéran.  
    •	Renversement de la situation au profit des Alliés en Extrême-Orient.  
    
    <iframe width="560" height="315" src="https://www.youtube.com/embed/B_T4aoS201A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    
    ## 1944
    
    En bref  
    •	Mars : les Japonais reculent dans le Pacifique.  
    •	Le Syndrome Z est découvert  
    •	6 juin : débarquement de Normandie.  
    •	22 juin : opération Bagration  
    •	22 juillet : accords de Bretton Woods.  
    •	15 août : débarquement de Provence.  
    •	25 août : libération de Paris.  
    •	Novembre : en Chine, à la suite de l'opération Ichi-Go, les Japonais ont pris toutes les bases de bombardement américaines, sauf Chongqing. Au Japon, les Américains bombardent les villes à partir des bases situées dans les îles Mariannes.  
    •	Le prix Nobel de la paix est décerné au Comité international de la Croix-Rouge (attribué rétroactivement en 1945).  
    [Plus d'info](https://www.youtube.com/watch?v=ShziFIuoeqs)
    
    ## 1945
    En bref  
    •	4 - 11 février : conférence de Yalta.  
    •	8 mai : capitulation de l’Allemagne.  
    •	Mai-juin : massacres de Sétif, Guelma et Kherrata.  
    •	Mai-juillet : crise franco-britannique au Levant.  
    •	17 juillet-2 août : conférence de Potsdam.  
    •	6 et 9 août : bombardements atomiques d'Hiroshima et Nagasaki.  
    •	2 septembre : capitulation du Japon.  
    •	20 novembre : ouverture du procès de Nuremberg.  
    •	3 décembre : le coût de l’armement qui a été utilisé pendant la Seconde Guerre mondiale est rendu public, soit plus de 1 000 milliards de dollars US.  
    
=== "English"
    
    It's a big topic isn't it? Especially during the third year of secondary school which was a pain in the ass but don't worry you've come to the right page!
    This is a year by year summary of the second world war with video from
    "History will tell us
    [Source](https://www.youtube.com/channel/UC2Cl2g2xFTZoAEldxYVzQFg), licence _Creative Commons_

    !!! donne "1939- World War II Volume 1"
    
        In brief  
        1 April: end of the civil war in Spain. Franco's dictatorship begins.  
        22 May: Pact of Steel.  
        23 August: German-Soviet pact.  
        1 September: Germany invades Poland. World War II begins.  
        3 September: The United Kingdom and France declare war on Germany.  
        30 November: The Russo-Finnish War begins.  
        
    
    !!! donne "1940"
    
        In brief  
        - 7 April: Norway campaign.  
        - 10-13 May: Sedan breakthrough.  
        - 10-28 May: invasion of Belgium by German troops.  
        - Appeal of 18 June.  
        - Armistice of 22nd June 1940.  
        - 3 July: Battle of Mers el-Kébir.  
        - 5 August: start of the Battle of Britain.  
        - In the night of 24 to 25 August: first German bombing of the city of London.  
        - 28 October: start of the Italo-Greek War.  
    
    !!! donne "1941"
    
        In brief  
        - 22 June: Operation Barbarossa.  
        - 14 August: Atlantic Charter.  
        - 7 December: attack on Pearl Harbor. Start of the Pacific War. Singapore, Malaya, the Philippines, Sarawak and North Borneo are invaded by the Japanese, who advance as far as Burma in the west, New Guinea in the south (1941-1942).  
        
    !!! donne "1942"
    
        In brief  
        - 26 May - 11 June: Battle of Bir Hakeim.  
        - 3 - 7 June: Battle of Midway.  
        - 16-17 July: Roundup of the Vélodrome d'Hiver.  
        - 7 August: Battle of Guadalcanal begins.  
        - 21 August: Battle of Stalingrad begins.  
        - 23 October - 3 November: Battle of El Alamein.  
        - 8 November: Operation Torch.  
          
    
    !!! donne "1943"
    
        In brief  
        - 14 January: Casablanca conference.  
        - 2 February: Soviet victory in the Battle of Stalingrad.  
        - 18 February: Sportpalast speech by Joseph Goebbels.  
        - 12 - 27 May: Trident conference.  
        - 27 May: Creation of the CNR (National Council of the Resistance)  
        - 5 July - 23 August: Battle of Kursk.  
        - 7 August - 2 October: Battle of Smolensk.  
        - 17 August: Quebec Conference.  
        - 2 - 9 September: start of the Italian campaign.  
        - 18 October - 11 November: Third Moscow Conference. 30 October: Moscow Declaration lays the foundations of the UN.  
        - 22 November: Cairo conference.  
        - 28 November: Teheran conference.  
        - Reversal of the situation in favour of the Allies in the Far East.  
    
    !!! donne "1944"
    
        In brief  
        - March: the Japanese retreat into the Pacific.  
        - Syndrome Z is discovered  
        - 6 June: Normandy landings.  
        - 22 June: Operation Bagration  
        - 22 July: Bretton Woods Agreement.  
        - 15 August: landing in Provence.  
        - 25 August: liberation of Paris.  
        - November: in China, following Operation Ichi-Go, the Japanese have taken all the American bombing bases except Chongqing. In Japan, the Americans bomb cities from bases in the Mariana Islands.  
        - The Nobel Peace Prize is awarded to the International Committee of the Red Cross (awarded retroactively to 1945).  

    !!! donne "1945"
    
        In brief  
        - 4 - 11 February: Yalta conference.  
        - 8 May: Germany capitulates.  
        - May-June: massacres of Sétif, Guelma and Kherrata.  
        - May-July: Franco-British crisis in the Levant.  
        - 17 July-2 August: Potsdam conference.  
        - 6th and 9th August: Atomic bombings of Hiroshima and Nagasaki.  
        - 2 September: Japan surrenders.  
        - 20 November: opening of the Nuremberg trial.  
        - 3 December: the cost of the armaments used during the Second World War is made public, i.e. more than 1,000 billion US dollars.  

=== "中国"
    
    第二次世界大战

    简而言之  
    4月1日：西班牙内战结束。佛朗哥独裁统治的开始。 
    5月22日：《钢铁之约》。  
    8月23日：德苏条约。  
    9月1日：德国入侵波兰。第二次世界大战开始。  
    9月3日：英国和法国向德国宣战。  
    11月30日：俄芬战争开始。  


    !!! donne "1940"

        简而言之  
        - 4月7日：挪威战役。  
        - 5月10-13日：轿车突破。  
        - 5月10-28日：德国军队入侵比利时。  
        - 6月18日的上诉。  
        - 1940年6月22日的停战协议。  
        - 7月3日：梅尔斯-凯比尔之战。  
        - 8月5日：不列颠之战的开始。  
            8月24日至25日夜间：德国对伦敦市进行了首次轰炸。  
        - 10月28日：意大利-希腊战争开始。  

    !!! donne "1941"  

        简而言之  
        - 6月22日：巴巴罗萨行动。  
        - 8月14日：《大西洋宪章》。  
        - 12月7日：对珍珠港的袭击。太平洋战争的开始。新加坡、马来西亚、菲律宾、沙捞越和北婆罗洲被日本人入侵，他们最远推进到西部的缅甸和南部的新几内亚（1941-1942）。  


    !!! donne "1942" 

        简而言之[  
        - 5月26日-6月11日：比尔-哈基姆之战。  
        - 6月3日至7日：中途岛之战。  
        - 7月16-17日：冬奥会圆环赛。  
        - 8月7日：瓜达尔卡纳尔岛之战开始。  
        - 8月21日：斯大林格勒战役开始。  
        - 10月23日-11月3日：阿拉曼战役（Battle of El Alamein）。  
        - 11月8日：火炬行动。  


    !!! donne "1943"

        简而言之   
        - 1月14日：卡萨布兰卡会议。  
        - 2月2日：苏军在斯大林格勒战役中取得胜利。  
        - 2月18日：约瑟夫-戈培尔在体育广场的演讲  
        - 5月12-27日：三叉戟会议。  
        - 5月27日：成立CNR（全国抵抗委员会）。  
        - 7月5日-8月23日：库尔斯克之战。  
        - 8月7日-10月2日：斯摩棱斯克之战。  
        - 8月17日：魁北克会议。  
        - 9月2-9日：意大利战役开始。  
        - 10月18日-11月11日：第三届莫斯科会议。10月30日：《莫斯科宣言》奠定了联合国的基础。  
    
    !!! donne "1944"
    
        简而言之  
        - 3月：日本人在太平洋地区撤退。  
        - Z综合征被发现  
        - 6月6日：诺曼底登陆。  
        - 6月22日：巴格拉季昂行动  
        - 7月22日：布雷顿森林协议。  
        - 8月15日：在普罗旺斯登陆。  
        - 8月25日：巴黎解放。  
        - 11月：在中国，继 "一哥行动 "之后，日本人占领了除重庆以外的所有美国轰炸基地。在日本，美国人从马里亚纳群岛的基地轰炸城市。  
        - 诺贝尔和平奖授予了红十字国际委员会（可追溯到1945年）。  
        
    !!! donne "1945"

        简而言之  
        - 2月4日至11日：雅尔塔会议。  
        - 5月8日：德国投降了。  
        - 5月至6月：在塞提夫、盖尔马和克尔拉塔发生大屠杀。  
        - 5-7月：法英两国在黎凡特的危机。  
        - 7月17日至8月2日：波茨坦会议。  
        - 8月6日和9日：广岛和长崎的原子弹爆炸。    
        - 9月2日：日本投降。    
        - 11月20日：纽伦堡审判开始。  
        - 12月3日：第二次世界大战期间使用的武器的成本被公开，超过1万亿美元。  
    

    
    
    
    
    