# Les raccourcis claviers sur Windows

## 1 | Les 10 raccourcis de base sur windows :

=== "Copier "

    !!! tip ""
    
        - ++ctrl+c++

=== "Coller"

    !!! tip ""

        - ++ctrl+v++

=== "Couper"

    !!! tip ""

        - ++ctrl+x++

=== "Annuler"

    !!! tip ""

        - ++ctrl+z++

=== "Rétablir"

    !!! tip ""

        - ++ctrl+y++

=== "Tout sélctionner"

    !!! tip ""

        - ++ctrl+a++

=== "Imprimer"

    !!! tip ""
    
        - ++ctrl+p++

=== "Afficher de l'aide"

    !!! tip ""
    
        - ++f1++

=== "Gestionnaire de Tache"

    !!! tip ""
    
        - ++ctrl+alt+delete++

=== "Menu Démarrer"

    !!! tip ""
    
        - ++windows++ ou ++ctrl+escape++


`Voici les 10 raccourcis de base sur Windows mais aussi les 10 plus utilisés dans le monde.`


## 2 | Les raccourcis Windows les plus utlisés par les professionels :

=== "Zoomer, Dé-zoomer"

    !!! tip ""
    
        - ++ctrl+mbutton++

=== "Changer le mode d'affichage"

    !!! tip ""
    
        - ++windows+p++  

=== "Lancer une nouvel instance"

    !!! tip ""
    
        - ++windows+shift+rbutton++

=== "Lancer une recherche rapide"

    !!! tip ""
    
        - ++windows+f++      

=== "Pour vérrouiller son ordinateur"

    !!! tip ""
    
        - ++windows+l++         

=== "Pour placer son curseur au début du mot"

    !!! tip ""
    
        - ++ctrl+right++ ou ++left++

=== "Pour sélectionner du texte"

    !!! tip ""
    
        - ++shift+right++ ou ++left++

=== "Pour sélectionner un mot entier"

    !!! tip ""
    
        -  ++ctrl+shift+right++ ou ++left++

=== "Pour supprimer un mot entier"

    !!! tip ""
    
        - ++ctrl+space++

=== "Pour fermer une fenêtre ou l'ordinateur"

    !!! danger "Si il y'a aucune fenêtre d'ouverte, ++alt+f4++ pourra éteindre votre ordinateur."
    
        - ++alt+f4++

=== "Pour afficher le poste de travail"

    !!! tip ""
    
        - ++windows+e++

=== "Empecher la lecture automatique d'un CD"

    !!! tip "Appuyer sur maj a l'insertion du CD"
    
        - ++shift++

=== "Pour rechercher un ordinateur sur un réeeau"

    !!! tip ""
    
        - ++ctrl+windows+f++

=== "Pour annuler le processus en cours"

    !!! tip "Cela marche principalement pour les transfert,copie..."
    
        - ++esc++

=== "Pour ouvrir en tant qu'administateur"

    !!! tip ""
    
        - ++ctrl+shift+rbutton++


## 3 | Les raccourcis claviers Windows pour gérer les fenêtres :

=== "Changer de fenêtre rapidement"

    !!! tip "Pour changer de fenêtre dans le sens inverse il faut faire ++alt+shift+tab++"
    
        - ++alt+tab++

=== "Pour masquer toutes les fenêtres"

    !!! tip "Pratique pour afficher brièvement le bureau, pour les avoir à nouveau il suffit juste de réappuyer sur ++windows+d++ .  "
    
        - ++windows+d++

=== "Pour déplacer une fenêtre"

    !!! tip "Mais vous pouvez aussi le faire avec les autres flèches de votre clavier.( ++up++ ++left++ ++right++ )"
    
        - ++windows+down++

=== "Que si vous avez deux écrans"

    !!! tip "Cela permet de déplacer une fenêtre d'un écran à l'autre."
    
        - ++windows+shift+right++ ou ++left++

## B | Une astuce pour les raccourcis sur les applications :

Sur les applications comme VSCodium vous avez la possibilité de voir les raccourcis interne à l'application. 

^^ex^^:  Aller sur **File** en haut à gauche de l'application et vous aurez à côté de **New File**, **New Window**, ... , plusieurs raccourcis qui vous feront gagner du temps.





