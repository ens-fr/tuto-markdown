# Le manga

## Logo

![logo](images/logo.png){width=300}
  
=== "Creation et histoire"  
    - _OHA_ est un manga crée par Thomas Fort en 2020. Son manga s'inspire de celui du manga _[My Hero Academia](https://fr.wikipedia.org/wiki/My_Hero_Academia)_ et se passe dans le même univers mais quelques années après. 
  
=== "Synopsis du manga"
    - Dans un monde où 80 % de la population mondiale possède des super-pouvoirs, ici nommés **_Alters_** , n'importe qui peut devenir un héros ou, s'il le souhaite, un criminel. Le manga suit les aventures de Thomas Fort, l'un des rares humains ne possédant pas d'Alter, qui rêve pourtant de devenir un jour un héros, il refuse d'accepter son manque d'alter au départ mais finira a l'age de 12 ans d'accepter son cas. Il abandonnera son projet de devenir un héros pour finalement apprendre le [parkour](https://fr.wikipedia.org/wiki/Parkour) se disant qu'il pourra quand même etre un peu utile avec ses talents de déplacement. En 3eme il fit une decouverte incroyable qui changea a jamais sa vie... Il découvrit qu'il possedait en fait 2 alter ,ceux de ces parents : L'eau et L'electicité. Avec ces alters il va tenter de devenir un héros et passera de justesse filière héroique au lycee Yuei, un lycee qui forme les plus grands héros. Il va devoir rattraper son retard sur les autres et apprendre rapidement a utliser ses alters.

 

