# Les Intégrales

## I - Intégrale et aire

??? tldr "1) Unité d'aire"

    Dans le repère ($O, I, J$), le rectangle rouge a comme dimension $1$ sur $1$. Il s'agit du rectangle « unité » qui a pour aire $1$
    unité d'aire. On écrit $1$ u.a. L'aire du rectangle vert est égale à $8$ fois l'aire du rectangle rouge.  
    L'aire du rectangle vert est donc égale à $8$ u.a. Lorsque les longueurs unitaires sont connues, il est possible de convertir les unités d'aire 
    en unités de mesure (le cm2 par exemple).

    ![Graph Unité d'aire](images/screen-unite-d'aire.png){width=400}

    - [ ] **Acquis**

??? info "2) Définition"

    :heart: **Définition :** Soit $f$ une fonction continue et positive sur un intervalle $[a ; b]$.
    On appelle **intégrale** de $f$ sur $[a ; b]$ l'aire, exprimée en u.a., de la surface délimitée
    par la courbe représentative de la fonction $f$, l'axe des abscisses et les droites 
    d'équations $x = a$ et $x = b$.

    ![Graph Unité d'aire](images/screen-def-d'aire.png){width=400}

    - [ ] **Acquis**

??? tldr "3) Notation"

    On lit « intégrale de $a$ à $b$ de $f(x) \ dx$ » et l'intégrale de la fonction $𝑓$ sur [$a$ ; $b$] se note :

    $$\int_{a}^b f(x) = dx$$

    Cette notation est due au mathématicien allemand Gottfried Wilhelm von Leibniz (1646 ; 1716).  
    Ce symbole fait penser à un « S » allongé et s'explique par le fait que l'intégral est égal à une aire calculée comme somme infinie d'autres aires.  
    Plus tard, un second mathématicien allemand, Bernhard Riemann (1826 ; 1866) établit une théorie aboutie du calcul intégral.

    :pushpin: **Remarques :**  

    - $a$ et $b$ sont appelés les bornes d'intégration.  
    - $x$ est la variable. Elle peut être remplacée par toute autre lettre qui n'intervient pas
    par ailleurs.  
    « $dx$ » ou « $dt$ » nous permet de reconnaître la variable d'intégration.  
    Ainsi on peut écrire : 

    $$\int_{a}^b f(x)dx = \int_{a}^b f(t)dt$$
    
    !!! example "Exemple"

        L'aire de la surface délimitée par la courbe représentative de la fonction $𝑓$ définie par :

        $$𝑓(𝑥) = 𝑥^2 + 1$$

        L'axe des abscisses et les droites d'équations $x = −2$ et $x = 1$ est
        l'intégrale de la fonction $𝑓$ sur l'intervalle $[-2 ; 1]$  
        et se note : 

        $$\int_{-2}^1 x^2 + 1dx$$

        ![Exemple Graph Unité d'aire](images/screen-exemple-unite-d'aire.png){width=400}

        !!! tip "Méthode :"

            :abacus: Calculer : $\int_{-2}^3 f(x)dx$

            ![Méthode Graph Unité d'aire](images/screen-exemple-unite-d'aire.png){width=400}

            Calculer cette fonction revient à calculer l'aire de la surface délimitée par la courbe
            représentative de la fonction f, l'axe des abscisses et les droites d'équations $x = −2$
            et $x = 3$.

            Donc par dénombrement et calculs d’aires de figures géométriques connues, on
            obtient :

            $$\int_{-2}^3 f(x)dx = 7u.a. + \frac{1}{2}u.a + \frac{2*1}{2}u.a = 8.5u.a.$$

    - [ ] **Acquis**

??? tldr "4) Encadrement de l'intégrale d'une fonction monotone et positive"

    Soit une fonction $f$ continue, positive et monotone sur un intervalle [$a$ ; $b$].  
    On partage l'intervalle [$a$ ; $b$] en n sous-intervalles de même amplitude.
    Sur chaque petit intervalle, on détermine la valeur maximale et minimale de la
    fonction $f$.  
    L’aire sous la courbe est alors encadrée par $2$ suites correspondantes à
    l’aire des rectangles rouges et des rectangles verts.  
    Ces deux suites $Ak$ et $Bk$ convergent vers la même limite :

    $$\int_{a}^b f(x)dx$$

    ![Encadrement Unité d'aire](images/screen-encadrement-d'aire.png){width=400}

    Ici sur l'intervalle [$0$ ; $1$], l'aire sous la courbe est comprise entre la somme des $n$
    rectangles « inférieurs » et la somme des $n$ rectangles « supérieurs ».  
    
    **:vhs: Voir cette [animation](https://www.geogebra.org/m/a8FgrUjJ).** 

    Voici un algorithme écrit en langage naturel permettant d'obtenir un tel encadrement.

    > Définir fonction $rectangle(a, b, n)$  

    > $L \leftarrow (b-a)/n$  
    > $x \leftarrow a$  
    > $m \leftarrow 0$  
    > $p \leftarrow 0$  

    > Pour i allant de 0 à $n-1$  
    > $m \leftarrow m+L×f(x)$  
    > $x \leftarrow x+L$  
    > $p \leftarrow p+L×f(x)$  
    > FinPour  

    > Afficher $m$ et $p$  

    **Exemple :**

    Avec Python, on programme l'algorithme pour la fonction : $𝑓(𝑥) = 𝑥^2.$

    ```python
    def rectangle(a, b, n):
        l = (b - a) / n
        x = a
        m = 0
        p = 0
        for i in range(0, n):
            m = m + l*x**2
            x = x + 1
            p = p + 1*x**2
        return m, p
    ```

    On exécute plusieurs fois le programme pour obtenir
    un encadrement de l'intégrale de la fonction carré sur [$1$ ; $2$].  
    En augmentant le nombre de sous-intervalles, la précision du calcul s'améliore car
    l'encadrement formé de rectangles inférieurs et supérieurs se resserre autour de la
    courbe.

    ```python
    >>> rectangle(1, 2, 10)
    (2.1850000000000014, 2.4850000000000017)
    >>> rectangle(1, 2, 50)
    (2.3034000000000017, 2.3634000000000017)
    >>> rectangle(1, 2, 100)
    (2.31835000000003, 2.3483500000000026)
    ```
    
    - [ ] **Acquis**

??? tldr "5)  Extension aux fonctions de signe quelconque"

    :heart: **Définition :** Soit $f$ une fonction continue sur un intervalle [$a$ ; $b$].  
    On appelle **intégrale** de $f$ sur [$a$ ; $b$] le nombre :

    $$𝐼 = \int_{a}^b f(x)dx$$

    Défini par :  
    - si $f$ est positive sur [$a$ ; $b$] : $I$ = Aire$(𝐸)$,  
    - si $f$ est négative sur [$a$ ; $b$] : $I$ = −Aire$(𝐸)$,  
    où $E$ est la surface délimitée par la courbe représentative de la fonction $f$, l'axe des
    abscisses et les droites d'équations $x = a$ et $x = b$.

    ![Extension Def Unité d'aire](images/screen-extension-def-d'aire.png){width=400}

    !!! example "Exemple :"

        $$𝐼 = \int_{2}^5 3-xdx = \frac{1x1}{2}-\frac{2x2}{2} = 1,5$$

        ![Extension Def Unité d'aire](images/screen-extension-def-exemple-d'aire.png){width=400}

    - [ ] **Acquis**

??? tldr "6) Propriétés"

    :bulb: **Propriétés :** Soit $f$ une fonction continue sur un intervalle $I$ et $a, b, c$ des réels de $I$.

    $$\int_{a}^a f(x)dx = 0$$

    $$\int_{a}^b f(x)dx = -\int_{a}^b f(x)dx$$

    **Relation de Chasles :  **

    $$\int_{a}^c f(x)dx + \int_{c}^b f(x)dx = \int_{a}^b f(x)dx$$

    :pushpin: **Remarque :**  

    Si une intégrale est nulle, alors la fonction n'est pas nécessairement nulle.  
    Par exemple :
    
    $$\int_{-2}^2 x^3dx = \int_{-2}^0 x^3dx + \int_{0}^2 x^3dx = 0$$

    La courbe représentative de la fonction cube est en effet symétrique par rapport à l’origine du repère, donc :

    $$\int_{-2}^0 x^3dx = -\int_{0}^2 x^3dx$$

    ![Propriétés remarque Unité d'aire](images/screen-proprietes-remarque-d'aire.png){width=300}
    
    !!! example "Exemple :"

        Reprenons le tout premier exemple :

        $$\int_{-2}^3 f(x)dx = \int_{-2}^-1 f(x)dx + \int_{-1}^1 f(x)dx + \int_{1}^3 f(x)dx = \frac{(1+2)×1}{2} + 2 × 2 + \frac{(1+2)×2}{2} = 8,5u.a$$ 

    - [ ] **Acquis**

## II - Intégrale et primitive

??? tldr "1) Fonction définie par une intégrale"

    :ledger: **Théorème :** Soit $f$ une fonction continue sur un intervalle [$a$ ; $b$].
    La fonction $f$ définie sur [$a$ ; $b$] par 

    $$𝐹(𝑥) = \int_{a}^x f(t)dt$$

    est la primitive de $f$ qui s’annule en $a$.

    ![Fonction théorème primitives](images/screen-fonction-theoreme-primitives.png){width=300}

    :ledger: **Théorème : Toute fonction continue sur un intervalle admet des primitives.**

    - [ ] **Acquis**
  
??? tldr "2) Calcul d’intégrales"

    :bulb: **Propriété :** Soit $f$ une fonction continue sur un intervalle [$a$ ; $b$].
    Si $F$ est une primitive de $f$ alors $\int_{a}^b f(x)dx = F(b) - F(a)$

    :heart: **Définition :**  

    Soit $f$ une fonction **continue** sur un intervalle $I$, $a$ et $b$ deux réels de $I$ et $F$
    une primitive de $f$ sur $[a ; b]$.  
    On appelle **intégrale** de $f$ sur [$a$ ; $b$] la différence $F(b) − F(a)$.  

    - [ ] **Acquis**

## III - Valeur moyenne d'une fonction

=== "Définition"

    Soit $f$ une fonction continue sur un intervalle [$a$ ; $b$] avec $a ≠ b$.  
    On appelle **valeur moyenne** de $f$ sur [$a$ ; $b$] le nombre réel :

    $$\frac{1}{b-a} \int_{a}^b f(x)dx$$

=== "Interprétation géométrique"

    L'aire sous la courbe représentative de $f$ (en rouge ci-dessous) est égale à l'aire sous la droite  
    d'équation $y = m$ (en bleu), entre $a$ et $b$.

    ![Valeur moyenne interprétationgéo](images/screen-valeur-moyenne-interpretationgeo.png){width=300}

=== "Méthode"

    :vhs: **[Vidéo](https://youtu.be/oVFHojz5y50)**  
    On modélise à l'aide d'une fonction le nombre de malades lors d'une épidémie.
    Au X-ième jour après le signalement des premiers cas, le nombre de malades est
    égale à  $f(x) = 16x^2 - x^3$

    Déterminer le nombre moyen de malades chaque jour sur une période de $16$ jours.
    
    $$m = \frac{1}{16-0} \int_{0}^16 f(x)dx$$

    $$m = \frac{1}{16} \int_{0}^16 16x^2 - x^3dx$$

    $$m = \frac{1}{16}(\frac{16}{3}×16^3 - \frac{1}{4} × 16^4 )$$

    $$m = \frac{1024}{3} = 341$$

    Le nombre moyen de malades chaque jour est environ égal à $341$.

    ![Méthode](images/screen-methode-valeur-moyenne.png){width=300}

- [ ] **Acquis**

## IV Calculs d'aire

=== "Propriété 1"

    Soit $f$ une fonctions continue sur [$a$ ; $b$] positive sur [$a$ ; $c$] et négative sur [$c$ ; $b$];

    $$\int_{a}^b f(x) \ dx = \mathscr A_{D1} - \mathscr A_{D2}$$

    où $D1$ est l’aire du domaine délimité par la courbe représentative de $f$ , l’axe des abscisses et les droites d’équation 
    $x = a$ et $x = c.$  
    $D2$ est l’aire du domaine délimité par la courbe représentative de $f$ , l’axe des
    abscisses et les droites d’équation $x = c$ et $x = b$.

    ![Graph Propriété](images/screen-calculs-d'aire-propriete.png){width=300}
    
=== "Propriété 2"

    Soit $f$ et $g$ deux fonctions continues sur un intervalle [$a$ ; $b$] telles que
    $g ≤ f$. Soit $D$ l’aire du domaine délimité par les courbes représentatives de $f$ et $g$, les droites
    d’équation $x = a$ et $x = b$.  

    Alors $D = \int_{a}^b f(x) - g(x)dx$

    ![Graph Propriété](images/screen-calculs-d'aire-propriete2.png){width=300}

=== "Méthode"

    :vhs: **[Vidéo](https://www.youtube.com/watch?v=oRSAYNwUiHQ)** 

    On considère les fonctions $f$ et $g$ définies par

    $$f(x) = x^2 + 1 et g(x) = -x^2 + 2x + 5$$

    On admet que pour tout $x$ de [$−1$ ; $2$], on a $f(x) ≤ g(x)$.  
    Déterminer l'aire délimitée par les courbes représentatives de $f$ et de $g$ sur
    l'intervalle [$−1$ ; $2$].

    $$A = \int_{-1}^2 g(x)dx - \int_{-1}^2 f(x)dx$$

    $$A = \int_{-1}^2 - x^2 + 2x + 5dx - \int_{-1}^2 x^2 + 1dx$$

    $$A = \int_{-1}^2 - x^2 + 2x + 5 - x^2 - 1dx$$

    $$A = \int_{-1}^2 - 2x^2 + 2x + 4 dx = ... = 9$$

    ![Screen méthode calculs d'aire](images/screen-methode-calculs-d'aire.png){width=300}

- [ ] **Acquis**

