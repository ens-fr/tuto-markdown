# CHEF DE PROJECT JEUX VIDEO

## Sommaire

1. [Sommaire](##sommaire)
2. [Introduction](##introduction)
3. [Fiche Technique](##fiche-technique)
4. [Qui est-ce ?](##qui-est-ce-?)
5. [Quel est sont rôle ?](##quel-est-sont-rôle-?)
6. [Que doit-il maîtriser](##que-doit-il-maîtriser-?)
7. [Quelles qualités doit-il posséder ?](##quelles-qualités-doit-il-posséder-?)
8. [Quel est le Bac à envisager ?](##quel-est-le-bac-à-envisager-?)
    1. [Comment faire des stages ?](##comment-faire-des-stages-?)
    2. [Quel est le salaire d’un chef de projet jeu vidéo ?](##quel-est-le-salaire-d'un-chef-de-projet-jeu-vidéo-?)
    3. [Quel évolution potentielle ?](##quel-évolution-potentielle-?)
    4. [Où travaille-t-il ?](##où-travaille-t-il-?)
    5. [Peut-il être freelance ?](##peut-il-être-freelance-?) 
9. [En résumé](##en-résumé)
10. [Comment devenir chef de projet jeu vidéo ?](##comment-devenir-chef-de-projet-jeu-vidéo-?)
11. [En vidéo](##en-vidéo)
12. [Métiers proches de chef de projet jeu vidéo](##métiers-proches-de-chef-de-projet-jeu-vidéo)
13. [Remerciements](##remerciements)
14. [Offre d'emploi, Stage ou Alternance](##offre-d'emploi,-stage-ou-alternance)
15. [Information](##information)
    1. [AFJV](###afjv)
    2. [Gaming Jobs](###gaming-jobs)
16. [Questionaire](##questionaire)

---

## Introduction

Touche-à-tout, impliqué durant tout le processus de création, le chef de projet jeu vidéo est celui qui garantit la réalisation d’un jeu vidéo. Il veille au budget, aux délais, mais surtout à la coordination entre tous les métiers impliqués dans le développement du jeu comme les game designers, graphistes, développeurs.

---

## Fiche Technique

Niveau d’études :	Bac+5
Bac conseillé :	Scientifique ou Economique
Employabilité :	Bonne
Salaire débutant :	2 895€
Salaire confirmé :	4 979€
Mobilité :	Bonne
Code ROME :	M1806
Code FAP :	M2Z90

---

## Qui est-ce ?

Il possède une vision générale du jeu et des métiers, tout en ayant les capacités à piloter et manager des équipes. Chef de projet jeu vidéo, c’est avant tout un métier où l’humain est au cœur de sa mission.

> « Le plaisir de ce métier est d'arriver à l'épanouissement des membres de l'équipe dans la réussite d'un projet dont nous sommes fiers », avance Pierre-Olivier Marec, co-fondateur et CTO du studio Mobbles.
> 
> ![Logo Mobbles](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/logo-mubble.png.webp)![Photo Pierre Olivier](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/marec.png.webp) PIERRE-OLIVIER MAREC ``Co-fondateur et CTO dustudio Mobbles``

---

## Quel est sont rôle ?

Le développement d’un jeu vidéo doit tenir compte de tout un ensemble de critères, dès lors les missions du chef de projet sont nombreuses. Il est capable de :

- Concevoir le projet
- Mettre en œuvre les méthodes
- Gérer la logistique (budget, effectifs, délais)
- Coordonner les activités des corps de métier
- Assurer l’interface entre la direction et ses équipes
- Gérer les imprévus et les risques
- Maîtriser les délais
- Respecter le budget
- Encadrer et animer les équipes
- Gérer les ressources humaines
- Veiller à la qualité des productions

>« Conduire un projet nécessite de travailler avec des équipes aux expertises variées. Il faut évoluer avec des développeurs, des artistes (graphistes, illustrateurs, designers), des game designers comme avec une équipe marketing. Chacune de ces expertises est vitale pour le développement d’un jeu vidéo, reconnaît Lucas Odion, chef de projet au sein de Revolt Games, studio éditeur du jeu vidéo Neopolis Game. Le chef de projet a ainsi la même fonction qu’un chef d’orchestre. »
> 
> ![Logo Neopolis](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/logo-neopolis.png.webp) ![Photo Lucas Odion](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/lucas-odion.png.webp) LUCAS ODION ``Chef de projet chez Neopolis``


![](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/Les-re%CC%81unions-en-e%CC%81quipe-sont-cle%CC%81es-pour-le-chef-de-projet-jeu-vide%CC%81o-e1616525890443.png.webp)

Les réunions en équipe sont clés pour le chef de projet jeu vidéo, ici la méthode scrum

---

## Que doit-il maîtriser ?

Polyvalent, le chef de projet jeu vidéo est en mesure d’appliquer ses compétences acquises à la fois en école spécialisée, mais aussi sur le terrain. Ainsi, il sait :

- Manager
- La technicité d’un jeu
- Les missions de chaque métier
- Dialoguer
- Décider
- Piloter dans l’ensemble le projet

---

## Quelles qualités doit-il posséder ?

> « En étant à l'écoute des contraintes métiers, des contraintes projet et des contraintes business, il pourra proposer des plans d'action et s'assurer de leur déroulé. Même s'il doit avoir une compréhension technique et être focalisé sur l'organisation du projet, le chef de projet est un métier très relationnel. Le jeu vidéo va au-delà d'un livrable technique, il s'approche souvent de l'œuvre d'art. Avoir une équipe harmonieuse permet de belles réalisations », souligne Pierre-Olivier Marec.
> 
> ![Logo Mobbles](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/logo-mubble.png.webp)![Photo Pierre Olivier](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/marec.png.webp) PIERRE-OLIVIER MAREC ``Co-fondateur et CTO dustudio Mobbles``

Si bien que les qualités pour occuper ce rôle demandent d’être :

- A l’écoute
- Avoir un sens de la communication et du relationnel
- Rigoureux
- Organisé

---

## Quel est le Bac à envisager ?

Un Bac spécialité économie ou mathématiques est conseillé avant de poursuivre en management ou commerce. Il est conseillé toutefois d’intégrer une école de jeux vidéo qui permet d’obtenir toutes les connaissances en matière de jeu vidéo, de ses métiers et du marché.

|QUELLES SONT LES FORMATIONS POSSIBLES ?|
|---------------------------------------|
| Si le poste de chef de projet demande de l’expérience, il ne peut être occupé en sortie d’études ([Licence pro métiers du jeu vidéo](https://gamingcampus.fr/diplomes/licence-pro-metiers-du-jeu-video.html)), mais seulement après quelques années sur le terrain. |
|C’est pourquoi avoir une formation qualifiante est l’idéale afin d’avoir des bases solides en la matière. Ce que propose G. Business, notre école située au sein de Gaming Campus, avec le  [MBA Management en jeux vidéo et esport](https://gaming.bs/mba-management-jeux-video-esport.html) **qui prépare à des postes à responsabilités**. Une formation en deux ans accessible de deux manières : après avoir suivi le [Bachelor Management jeux vidéo et esport](https://gaming.bs/bachelor-management-jeux-video-esport.html) de l’école ou en admission parallèle (après un Bac +3 ou un Bac +4 validé). |
| Tout l’univers du jeu vidéo y est enseigné, avec des stages qui complètent la formation. Il est également envisageable de passer par le [Bachelor](https://gaming.tech/#formations) puis le [MSc Programmation jeu vidéo de G. Tech](https://gaming.tech/#formations). Le premier sur trois ans et le second sur 2 ans donnent toutes les clés pour répondre aux besoins techniques des studios. |

[En Savoir Plus](https://gaming.tech/){ .md-button }

----

## Comment faire des stages ?

Toutes nos écoles proposent des périodes de stages durant les formations. Des immersions en entreprise qu’il est important de faire afin de mettre en application ses connaissances et de se familiariser avec le monde du gaming. De plus, cela ajoute des lignes sur un CV.

De deux mois à six mois en passant par une dernière année de MBA / MSc à rythme alterné de CDD et stage, nous proposons que l’étudiant, à chaque fin d’année de formation, se retrouve dans un studio de développement de jeux vidéo. Pour faciliter les démarches, nous publions des offres de stages.

![Une bonne gestion de la réalisation](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/Exemple-de-projet-re%CC%81ussi-Minecraft-e1616525911847.png.webp)
Une bonne gestion de la réalisation d’un jeu peut donner des succès à l’image de Minecraft

---

## Quel est le salaire d’un chef de projet jeu vidéo ?

**En début de carrière** (après [un Bachelor Développeur de jeux vidéo](https://gamingcampus.fr/diplomes/bachelor-developpeur-jeux-video.html)), un chef de projet jeu vidéo verra sa rémunération s’afficher autour de 3 000 euros mensuels suivant l’entreprise pour laquelle il travaille. Après plusieurs années, son salaire peut facilement atteindre plus de 70 000 euros annuels. Si l’envie de vous exiler à Montréal vous intéresse, le salaire d’un chef de projet s’affiche en moyenne à 75 000 dollars et peut dépasser facilement les 120 000 dollars. Aux Etats-Unis, c’est pratiquement la même chose avec une fourchette de rémunérations comprise entre 54 000 et 113 000 dollars.

---

## Quel évolution potentielle ?

Grâce à sa vision d’ensemble dans la production d’un jeu vidéo et avec ses compétences en matière de management (acquises en [MBA / Mastère Production jeux vidéo](https://gamingcampus.fr/diplomes/mba-mastere-production-jeux-video.html)) , un chef de projet peut évoluer vers d’autres postes à responsabilités – dans le jeu vidéo ou non -, comme **directeur de production, producteur multimédia ou consultant**.

---

## Où travaille-t-il ?

Le chef de projet travaille pour une entreprise du jeu vidéo. De la petite start-up au studio mondialement connu, tout le monde a son chef de projet, voire en à plusieurs.

---

## Peut-il être freelance ?

Dans le jeu vidéo, nombreux sont les métiers qui peuvent s’occuper en freelance. Cependant, il y en a certains qui le sont moins. C’est le cas du chef de projet. Les entreprises aiment bien avoir des profils plutôt en interne que des freelance. Ce n’est toutefois pas impossible. Il faudra faire preuve de patience et rouler sa bosse sur de nombreux projets pour le devenir. En effet, les clients regarderont en premier les références et les compétences avant de vous embaucher.

![Start-up](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/Le-chef-de-projet-jeu-vide%CC%81o-peut-travailler-dans-une-start-up-ou-dans-un-studio-de-de%CC%81veloppement-de-jeux-vide%CC%81o-1-e1616525929701.png.webp)
Le chef de projet jeu vidéo peut travailler dans une start-up ou dans un studio de développement de jeux vidéo

---

Avantages
- Salaire important en milieu de carrière
- Mobilité internationale
- Connaissances très larges du secteur du jeu vidéo

Inconvénients
- Implication forte
- Investissement en temps important
- Employabilité plus faible

---

## En résumé

![Résumé](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/Re%CC%81sume%CC%81-fiche-me%CC%81tier-chef-de-projet-jeu-vide%CC%81o-e1616825529453.png.webp)


> 👑 Quelles sont les missions du chef de projet jeu vidéo ?
    Durant la création d’un jeu vidéo, coordonner les équipes impliquées, penser aux  éthodes pour parvenir au résultat final, s’assurer que les délais soient spectés, tout comme le budget, c’est au chef de projet jeu vidéo qu’il revient de mener ces actions. A savoir qu’il peut aussi être spécialisé en technique, business,  réation selon la taille des studios.
>
> Un métier qui demande pas mal de compétences notamment dans la chaîne de       production d’un jeu et autant de connaissances dans les missions de chaque métier. Il lui faut aussi être suffisamment armé pour manager et piloter l’ensemble du projet. C’est pourquoi, une formation et de l’expérience sont nécessaires.


> 💰 Quel est le salaire du chef de projet jeu vidéo ?
> Tout dépend de son niveau d’expérience. Mais le salaire d’un chef de projet jeu vidéo peut aller de 30 000 euros brut annuels en début de carrière, soit 2 500 euros brut par mois à plus 40 000 euros voire même 70 000 euros par an après plusieurs années d’expérience. Il s’agit là d’estimation selon les observations.
>
>C’est à l’étranger qu’un chef de projet jeu vidéo gagnera plus avec en moyenne 75 000 dollars par an à Montréal (jusqu’à 120 000 même). Aux Etats-Unis, on constate une rémunération comprise entre 54 000 et 113 000 dollars annuels.


>🧠 Quel niveau d’étude pour devenir chef de projet jeu vidéo ?
>Chef de projet jeu vidéo, ce n’est pas le genre de métier que l’on peut faire dès la fin de ses études. La raison est simple : il faut un bagage suffisant en matière de gestion de projet et d’équipe.
>
>Donc, avant toute chose, il faudra passer par un Bac +5 afin de pouvoir maîtriser les aspects techniques, managériaux, marketing, communication. En gros, toute la production d’un jeu. Il est important d’avoir un bon niveau car cela conditionnera ensuite le parcours professionnel.


>🎓 Quel Bac choisir pour devenir chef de projet jeu vidéo ?
>Pour postuler à une formation en post-bac pour devenir chef de projet jeu vidéo, rien de tel que d’avoir un Bac général avec pour spécialités les sciences économiques par exemple ou bien le numérique et les sciences informatiques.
>
>On conseille même la spécialité mathématiques. Un niveau qui permet ainsi d’avoir le sens de la logique et de l’organisation. Des caractéristiques importantes à ce poste.


>📚 Quelle est la formation pour devenir chef de projet jeu vidéo ?
Avoir une formation de niveau Bac +5 est incontournable. C’est donc ce que nous vous proposons dans nos écoles de Gaming Campus (situées à Lyon et Paris). A G. Business, vous pourrez d’abord passer par le [Bachelor Management jeux vidéo et esport](https://gaming.bs/bachelor-management-jeux-video-esport.html) puis poursuivre en [MBA Management en jeux vidéo et esport](https://gaming.bs/mba-management-jeux-video-esport.html). L’occasion d’apprendre les bases du management et de la gestion de projet, entre autres, et de vous perfectionner afin de sortir avec une maîtrise parfaite de ces domaines. Après quelques années d’expérience, comme assistant par exemple, vous pourrez ensuite prendre des responsabilités plus importantes.
>
>Nous proposons aussi à G. Tech, notre école d’informatique, de passer par le [Bachelor](https://gaming.tech/#formations) puis le [MSc Programmation jeu vidéo](https://gaming.tech/#formations) afin d’être capable de conduire le projet sur la partie développement.
>
>A chaque fois, des stages viennent compléter le cursus.

>🎮 Comment devenir chef de projet jeu vidéo ?
>Ce poste là, s’il demande un certain niveau de formation, il requiert également plusieurs années d’expérience. En effet, il est difficile de devenir chef de projet jeu vidéo simplement en sortant d’étude. On vous demandera forcément des références. Finalement, il fait partie des métiers que l’on occupe en évoluant dans sa carrière.
>
>Pour pouvoir y parvenir, prenez le temps de vous former en MBA / MSc au sein de l’une des écoles de Gaming Campus. Vous obtiendrez ainsi toutes les compétences techniques et les connaissances du secteur.

---

## Comment devenir chef de projet jeu vidéo ?

Véritable chef d’orchestre au sein d’un studio, le chef de projet vidéo pilote et coordonne l’ensemble de la création du jeu. La bonne gestion de sa réalisation repose donc sur ses épaules. Métier à responsabilités, il demande alors de posséder toutes les facultés pour mener à bien sa mission. Pour devenir **chef de projet jeu vidéo**, il faut quelques années d’expérience. Commencer d’abord par un **Bachelor Management jeux vidéo et esport** à l’école [G. Business](https://gaming.bs/), puis s’orienter vers le **MBA Management en jeu vidéo**. Un diplôme qui prépare à des postes à responsabilités. Il est possible aussi d’opter pour le **MSc en Programmation jeu vidéo** de [G. Tech](https://gaming.bs/).

---

## En vidéo
[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/9Ay4b47NMRY/maxresdefault.jpg)](https://youtu.be/9Ay4b47NMRY)

<iframe width="720" height="720" src="https://youtu.be/9Ay4b47NMRY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Métiers proches de chef de projet jeu vidéo

Continuez vos recherches autour des métiers de chef de projet jeu vidéo :
=== "Programmeur de Jeux Video"
    ![Programmeur de jeux video](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/Programmeur-jeu-video.png.webp)

=== "Chef de produit Jeux Video"
    ![Chef de produit Jeux Video](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/Chef-de-produit-jeu-video-esport.png.webp)

=== "Chef de Project Esport"
    ![Chef de Project Esport](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/Chef-de-projet-esport-e1610230017357.png.webp)

---

## Remerciements

Cette fiche métier « Chef de projet jeu vidéo » a été réalisée avec l’aide de professionnels du secteur du jeu vidéo. Découvrez leur entreprise :

=== "Pierre Olivier Marec"
    ![Logo Mobbles](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/logo-mubble.png.webp)![Photo Pierre Olivier](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/marec.png.webp)

    **Pierre Oliver Marec**  
    CTO Mobble  
    [Site Internete](https://www.mobbles.com/)
=== "Lucas Odin"
    ![Logo Neopolis](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/logo-neopolis.png.webp) ![Photo Lucas Odion](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/lucas-odion.png.webp)

    **Lucas Odin**  
    Chef de Porject Néopolis  
    [Site Internete](https://www.neopolisgame.com/)

---

## Offre d'emploi, Stage ou Alternance

Découvrez les nouvelles offres d'emploi, de stages ou d'alternance autour du métier de chef de projet jeux video. Nous vous proposons chaque jour les meilleures offres de chef de projet jeux video en partenariat avec les deux sites d'emploi du jeu vidéo leaders en France : AFJV (Agence Française pour le Jeu Vidéo) et Gaming Jobs.

---

## Information

### AFJV

**Assistant(e) Chef de Projet DIgital** -  Stage -  APPERTURE -  26 Mai 2021
**Release Manager (H/F)** - CDI -  FOCUS HOME INTERACTIVE -  25 Mai 2021
**Technical Documentation Manager (H/F)** -  CDI -  QUANTIC DREAM -  24 Mai 2021
**Mobile Game Producer / Chef de Projet mobile (H/F)** - CDI -  QUANTIC DREAM -  19 Mai 2021
**Game Producer Lille ou Paris** - CDI -  NACON -  12 Mai 2021

[Consulter les offres](https://emploi.afjv.com/index.php){ .md-button }

### Gaming Jobs

**Proposition de bénévolat de Caster FPS** -  Bénévolat -  KRAKENS EMPIRE -  15 Mai 2021
**RESPONSABLE DÉVELOPPEMENT PROJET ACCESSOIRES GAMING** - CDI -  NACON -  15 Mai 2021

[Consulter les offres](https://fr.jobs.game/offres-production/){ .md-button }

=== "Découvrez les nouveaux talents du jeu vidéo"
    ROOKIES
    Découvrez les nouveaux talents du jeu vidéo
    Premiers diplômés de notre école de commerce G. BS, découvrez les profils et les parcours de nos alumnis 2020. Une première promotion regroupant les MBA suivants :  
    ➔ MBA Entrepreneuriat, Management de projet et de l’innovation du jeu vidéo  
    ➔ MBA Marketing, communication & event management  
    ➔ MBA Esports Business & Management  

    [Voir les ROOKIES](https://gamingcampus.fr/rookies-2021.html){ .md-button }

=== "Téléchargez le « starter pack » Gaming Campus"
    Téléchargez le « starter pack » Gaming Campus
    Faites le plein d'informations sur le secteur du jeu vidéo, les métiers, l'emploi et les formations possibles. Réalisé avec nos partenaires l'AFJV, Gaming Jobs, le SNJV, Les Echos Start et de nombreux professionnels.  
    ➔ Le Guide des Métiers du Jeu Vidéo et de l'esport  
    ➔ Le Tomorrow Lab 2019-2020  
    ➔ Le Baromètre Annuel du Jeu Vidéo en France  
    ➔ L’Observatoire de l’Emploi du Jeu Vidéo  
    ➔ La plaquette des formations Gaming Campus  
    
    [Télécharger](https://gamingcampus.fr/deck.html){ .md-button }

=== "MÉDIAS"
    Ils parlent de nous  
    
    | Le Parisien | Les Echos | L'équipe | Le Figaro |
    |-------------|-----------|----------|-----------|
    | Challenges  | Le progrés| BFMTV | Le Monde |

    [Voir tout les retombes de presse](https://gamingcampus.fr/presse.html){ .md-button }

=== "Partenaire Oficielle"
    | Crédit Mutuel | OMEN | Maxnomic | Materiel.net |
    |-------------|-----------|----------|-----------|
    | FC NATES | MCES | Micromania Zing | Early Maker |
    

=== "ENTREPRISES QUI PARTAGENT LEUR EXPÉRIENCE AVEC NOS ÉTUDIANTS"
    ENTREPRISES QUI PARTAGENT LEUR EXPÉRIENCE AVEC NOS ÉTUDIANTS  

    ![](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/Logo-aws.png.webp)![](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/Logo-SELL.png.webp)![](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/logo-Google.png.webp)![](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/logo-team-bds.png.webp)![](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/logo-ubisoft.png.webp)![](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/Logo-Gameonly.png.webp)![](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/psg.jpg.webp)![](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/ea.jpg.webp)![](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/esl.jpg.webp)![](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/smartvr.jpg.webp)![](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/vitality.png.webp)![](https://gamingcampus.fr/wp-content/webp-express/webp-images/uploads/2020/05/webedia.jpg.webp)

    [Voir les 200+ Entreprises](https://gamingcampus.fr/ecoles.html#bloc__entreprises){ .md-button }

---

## Questionaire

=== "Question"
    - [ ] Bien Compris
    - [ ] Compris
    - [ ] Peu Compris
    - [ ] Pas Comprie

=== "Réponse"
    - ✅ Bien Compris
    - ✅ Compris
    - ❌ Peu Compris
    - ❌ Pas Comprie
    Mais bien sur quoi d'autre.

Merci à Tout