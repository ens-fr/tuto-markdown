lien pour cette leçon sur français facile : [cliquer ici](https://www.francaisfacile.com/exercices/exercice-francais-2/exercice-francais-37534.php)

# Conditionnel présent

Comme l'indicatif et le subjonctif, le conditionnel est un **mode**.  
On l'utilise pour exprimer une action qui aura lieu à condition qu'une autre action ait pu avoir lieu avant.  
_Exemple : Si Jordan gagnait au loto, il s'achèterait une voiture de sport rouge._

On emploie le conditionnel présent ("_achèterait_") lorsque la condition est exprimée à l'imparfait (_si Jordan gagnait_).

Le conditionnel présent s'emploie aussi :

- pour demander quelque chose poliment.  
_Exemple : Pourriez-vous fermer la porte s'il vous plaît ?_

- pour donner une information, sans certitude.  
_Exemple : Il se pourrait qu'il pleuve demain._

## Formation

Au conditionnel présent, le **radical** est celui du **futur simple de l'indicatif** et les **terminaisons** sont celles de l'**imparfait**.

!!! info "Terminaisons"
    -ais, -ais, -ait, -ions, -iez, -aient

!!! Example "Exemples"
    === "Chanter"
        Je chanterais  
        Tu chanterais  
        Il/Elle/On chanterait  
        Nous chanterions  
        Vous chanteriez  
        Ils/Elles chanteraient 
    === "Choisir"
        Je choisirais  
        Tu choisirais  
        Il/Elle/On choisirait  
        Nous choisirions  
        Vous choisiriez  
        Ils/Elles choisiraient 
    === "Boire"
        Je boirais  
        Tu boirais  
        Il/Elle/On boirait  
        Nous boirions  
        Vous boiriez  
        Ils boiraient 
    === "Avoir"
        J'aurais  
        Tu aurais  
        Il/Elle/On aurait  
        Nous aurions  
        Vous auriez  
        Ils/Elles auraient 
    === "Être"
        Je serais  
        Tu serais  
        Il/Elle/On serait  
        Nous serions  
        Vous seriez  
        Ils/Elles seraient 

## Vidéo de la leçon
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/JRqW73GXWl4?start=101" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
[source](https://www.youtube-nocookie.com/embed/JRqW73GXWl4?start=101)
## Exercice

Conjuguez le verbe entre parenthèse.

=== ":one:"
    Il _(aimer)_ bien devenir une star.  

    * [ ] aimerait
    * [ ] aimerais
    * [ ] aimerai

    ??? done "correction"
        :heavy_check_mark: aimerait

=== ":two:"
    Si elles se rencontraient au supermarché, elles _(bavarder)_ longtemps.

    * [ ] bavarderait
    * [ ] bavarderiez
    * [ ] bavarderaient

    ??? done "correction"
        :heavy_check_mark: bavarderaient

=== ":three:"
    Si j'avais un chien, je le _(promener)_ chaque jour. 

    * [ ] promenerais
    * [ ] promenerait
    * [ ] promenerrais

    ??? done "correction"
        :heavy_check_mark: promenerais

=== ":four:"
    Si nous étions plus attentifs en cours, nous _(finir)_ plus vite nos devoirs.

    * [ ] finirons
    * [ ] finiriez
    * [ ] finirions

    ??? done "correction"
        :heavy_check_mark: finirions

=== ":five:"
    Si vous gagniez de l'argent, je parierais que vous le _(dépenser)_ trop vite. 

    * [ ] dépenserais
    * [ ] dépenserait
    * [ ] dépenserai

    ??? done "correction"
        :heavy_check_mark: dépenserais

=== ":six:"
    Si tu aimais bien les haricots verts, tu en _(manger)_ souvent.

    * [ ] mangerai
    * [ ] mangerais
    * [ ] mangerait

    ??? done "correction"
        :heavy_check_mark: mangerais

=== ":seven:"
    Si mes élèves faisaient du bruit, j'_(insister)_ pour obtenir le silence.    

    * [ ] insisterait 
    * [ ] insisterai
    * [ ] insisterais

    ??? done "correction"
        :heavy_check_mark: insisterais 

=== ":eight:"
    _(pouvoir)_ -vous me passer le sel s'il vous plaît ? 
  
    * [ ]  pouriez
    * [ ]  pourriais
    * [ ]  pourriez

    ??? done "correction"
        :heavy_check_mark:  pourriez

=== ":nine:"
    Il _(se souvenir)_ de moi si j'étais plus souvent avec lui.  

    * [ ] se souviendrais
    * [ ] se souviendrai
    * [ ] se souviendrait

    ??? done "correction"
        :heavy_check_mark: se souviendrait

=== ":keycap_ten:"
    _(pouvoir)_-je avoir le silence s'il vous plaît ? 
  
    * [ ] pourais
    * [ ] pourrait
    * [ ] pourrais

    ??? done "correction"
        :heavy_check_mark: pourrais
    