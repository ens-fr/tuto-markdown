lien pour cette leçon sur français facile : [cliquer ici](https://www.francaisfacile.com/exercices/exercice-francais-2/exercice-francais-53549.php)


# Futur simple - cours

C'est un temps qui est employé principalement pour parler d'une action à venir.

## Formation

Les terminaisons sont les mêmes pour tous les verbes : **-ai, -as, -a, -ons, -ez, -ont**

### Verbes du 1er groupe

On ajoute (en général) la terminaison à l'infinitif du verbe.

!!! example "Exemples"
    === "Jouer"
        Je jouer**ai**   
        Tu jouer**as**   
        Il/Elle/On jouer**a**   
        Nous jouer**ons**  
        Vous jouer**ez**  
        Ils/ELles jouer**ont**

    === "Se laver"
        Je me laver**ai**	
        Tu te laver**as**  
        Il/Elle/On se laver**a**  
        Nous nous laver**ons**	
        Vous vous laver**ez**  
        Ils/Elles se laver**ont**


!!! warning "Les verbes en -yer changent le y en i devant un e muet"
    essuyer, nettoyer, payer, ployer, appuyer...  
    Tu essuieras la vaisselle, Nicolas balaiera le séjour.  
    !!! info "Pour les verbes en -ayer il est permis de garder le y"
        Nicolas balayera le séjour.
    exception : envoyer → j'enverrai (même chose pour : "renvoyer")

!!! warning "Les verbes en -eler et -eter doublent la consonne l ou t devant un e muet"

    je jetterai, tu appelleras, vous épellerez

    !!! info "Seuls quelques verbes ne doublent pas la consonne et s'écrivent avec un accent grave"
        je pèlerai, tu achèteras 
        Les plus utilisés sont :

        * acheter
        * celer
        * ciseler	
        * démanteler	
        * écarteler
        * fureter	
        * geler	
        * marteler	
        * modeler
        * peler

    

### Verbes du 2e groupe

On ajoute les terminaisons à l'infinitif du verbe.

!!! example "Exemples"
    === "Obéir"
        J'obéir**ai**   
        Tu obéir**as**   
        Il/Elle/On obéir**a**   
        Nous obéir**ons**  
        Vous obéir**ez**  
        Ils/ELles obéir**ont**

    === "Pâlir"
        Je pâlir**ai** 	
        Tu pâlir**as**  
        Il/Elle/On pâlir**a**  
        Nous pâlir**ons**	
        Vous pâlir**ez**  
        Ils/Elles pâlir**ont**


### Verbes du 3e groupe

Pour certains des verbes du 3e groupe, la formation est la même que pour les deux premiers groupes, 
mais il y a des irrégularités.

!!! warning "Les verbes dont l'infinitif se termine par 'e' perdent cette lettre au futur"
    !!! example "Exemples"

        |Infinitif|Verbe conjugué|
        |---|---|
        |prendre|je prendrai|
        |répandre |tu répandras	 |
        |boire |il boira|
        |rire|nous rirons|
        |conclure|vous conclurez|
        |naître|ils naîtront|

!!! warning "Certains verbes en -ir (et leurs composés) perdent le 'i' "
    !!! example "Exemples"
        |Infinitif|Verbe conjugué|
        |---|---|
        |acquérir |j'acquerrai|
        |	courir |tu courras|
        |mourir |il mourra|

!!! warning "Autres irrégularités"
    Le radical est très différent de l'infinitif mais les terminaisons reste les mêmes.
    === "être"
        Je serai  
        Tu seras  
        Il/Elle/On sera  
        Nous serons  
        Vous serez  
        Ils/Elles seront  

    === "avoir"
        J'aurai	 
        Tu auras	
        Il aura  
        Nous aurons	  
        Vous aurez	  
        Ils auront  

    === "aller"
        J'irai	  
        Tu iras	  
        Il ira  
        Nous irons	  
        Vous irez	  
        Ils iront  

    Quelques-uns des verbes irréguliers

    |Infinitif|1er personne|
    |---|---|
    |faire|je ferai|
    |pouvoir|je pourrai|
    |voir|je verrai|
    |recevoir|je recevrai|
    |savoir|je saurai|
    |tenir|je tiendrai|
    |venir|je viendrai|
    |asseoir|j'assoirai|
    |devoir|je devrai|
    |vouloir|je voudrai|
    |valoir|je vaudrai|
    |cueillir|je cueillerai|

    !!! info "Les verbes défectifs"
        "falloir" et "pleuvoir" sont des verbes défectifs  
        "falloir" ne se conjugue qu'à la troisième personne du singulier.  
        "pleuvoir" ne se conjugue qu'à la troisième personne (singulier et pluriel)  

## Vidéo de la leçon

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/GfsrhmMdA5w?start=101" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
[source](https://www.youtube-nocookie.com/embed/GfsrhmMdA5w?start=101)

## Exercice

Conjuguez le verbe entre parenthèse au futur.

=== ":one:"
    ![Image illustrant la phrase](https://www.anglaisfacile.com/cgi2/myexam/images/21310.jpg "image de l'exercice 1")  
    Dimanche, nous _(aller)_ chez Mamie, et nous  _(cueillir)_ des cerises dans son jardin.  
    
    Aller

    * [ ] allerons
    * [ ] iront
    * [ ] irons

    Cueillir

    * [ ] cueilleront
    * [ ] cueillerons
    * [ ] cueillirons

    ??? done "correction"
        :heavy_check_mark: irons  
        :heavy_check_mark: cueillerons  

=== ":two:"
    ![Image illustrant la phrase](https://www.anglaisfacile.com/cgi2/myexam/images2/27236.jpg "image de l'exercice 2")  
    Pour le moment, tu es beaucoup trop jeune mais quand tu _(avoir)_ 18 ans tu _(pouvoir)_ conduire comme ta sœur.  

    avoir

    * [ ] auras
    * [ ] aura
    * [ ] aurras

    pouvoir

    * [ ] pourras
    * [ ] poura
    * [ ] pouras

    ??? done "correction"
        :heavy_check_mark: auras  
        :heavy_check_mark: pourras

=== ":three:"
    ![Image illustrant la phrase](https://www.anglaisfacile.com/cgi2/myexam/images/20771.jpg "image de l'exercice 3")  
    Les enfants, en rentrant de l'école, vous _(faire)_ d'abord vos devoirs, vous ne _(jouer)_ avec la console que lorsqu'ils seront terminés. 
  
    faire

    * [ ] fairez
    * [ ] ferrez
    * [ ] ferez

    jouer

    * [ ] jouerrez
    * [ ] jouerai
    * [ ] jouerez

    ??? done "correction"
        :heavy_check_mark: ferez  
        :heavy_check_mark: jouerez

=== ":four:"
    ![Image illustrant la phrase](https://www.anglaisfacile.com/cgi2/myexam/images/18665.jpg "image de l'exercice 4")  
    Demain, j'_(envoyer)_ un joli bouquet de fleurs à tante Léna pour son anniversaire.  

    envoyer

    * [ ] enverai
    * [ ] envoyerai
    * [ ] enverrai

    ??? done "correction"
        :heavy_check_mark: enverrai

=== ":five:"
    ![Image illustrant la phrase](https://www.anglaisfacile.com/cgi2/myexam/images2/26584.jpg "image de l'exercice 5")  
    David a travaillé très sérieusement. Je pense qu'il _(obtenir)_ de bons résultats à ses examens. Quand les enfants _(savoir)_ faire la première question, ils _(être)_ capables de réussir l'exercice. 
  
    obtenir

    * [ ] obtiendra
    * [ ] obteniras
    * [ ] obtenirra

    savoir

    * [ ] saurons
    * [ ] saurront
    * [ ] sauront

    être

    * [ ] seront
    * [ ] serrons
    * [ ] serons

    ??? done "correction"
        :heavy_check_mark: obtiendra  
        :heavy_check_mark: sauront  
        :heavy_check_mark: seront

=== ":six:"
    ![Image illustrant la phrase](https://www.anglaisfacile.com/cgi2/myexam/images2/27856.gif "image de l'exercice 6")  
    Si j'observe le ciel très longtemps, je _(voir)_ peut-être une nouvelle étoile, et je _(devenir)_ célèbre.    
  
    voir

    * [ ] verrai
    * [ ] verai
    * [ ] verrais

    devenir

    * [ ] devenirai
    * [ ] deviendrai
    * [ ] deviendrais

    ??? done "correction"
        :heavy_check_mark: verrai  
        :heavy_check_mark: deviendrai
