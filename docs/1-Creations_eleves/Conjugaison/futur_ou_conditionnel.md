lien pour cette leçon sur français facile: [cliquer ici](https://www.francaisfacile.com/exercices/exercice-francais-2/exercice-francais-125884.php)



# Futur simple ou conditionnel présent - cours

## L'utilisation du futur simple

Demain ; la semaine prochaine ; Dans deux minutes ; dès que possible ; prochainement ;
ultérieurement ; par la suite ; à l'avenir ; un de ces jours ...

|Les valeurs du futur simple|Exemples|
|---|---|
|Le futur simple indique une **action** qui se fera **dans l'avenir**. |**Je serai** absente du 19 au 24 mai. Demain, **je tiendrai** compagnie à Mme F.|
|**Injonction polie**, pour donner un ordre de manière polie  |**Vous n'oublierez pas** de fermer la porte en sortant, merci.|
|Dans des **situations** formelles ou **d'annonces**.|Le président de la République **se rendra** en Chine prochainement.|
|Envisager une **hypothèse**, dont  la condition est **réalisable**|**Je suivrai** la formation en ligne.|

##  L'utilisation du conditionnel présent

|Valeurs du conditionnel présent| Exemples|
|---|---|
|Une **demande polie**, atténuée.|**Pourrais-tu** m'envoyer ces documents rapidement s'il te plaît ?   **Pourriez-vous** me renseigner ? **Je vous serais reconnaissante** de bien vouloir me transmettre ces documents.|
|Un **souhait**|**Je souhaiterais obtenir** davantage de renseignements, s'il vous plaît. **Je préférerais** passer l'entretien jeudi prochain.|
|Un **conseil**|**Tu devrais commencer** à chercher ton stage dès le début de la formation. À votre place, **je partirais** quelques jours.|
|Une hypothèse dont **la condition est possible, éventuelle, peu probable mais réalisable.**|S'il s'arrêtait de pleuvoir, **j'irais** faire des courses.|
|Une **hypothèse irréalisable**, un **irréel du présent**, _on imagine un présent qui ne pourra peut-être jamais exister._|Si tu étais un animal, **que serais-tu ?** Si je connaissais la règle, **je l'appliquerais**. Si j'avais un ordinateur, **je pourrais** suivre la formation en ligne.|
|Un **projet hypothétique**, une **suggestion**.|**Ce serait** bien si nous allions [...] ; **Il serait intéressant** de collecter des informations sur [...] ; Cette initiative **permettrait** de gagner du temps.|

## Vidéo de la leçon
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/PBmOL6cUYi8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
[source](https://www.youtube.com/watch?v=PBmOL6cUYi8)

## Exercice

Conjuguez le verbe entre parenthèse.

=== ":one:"
    Veuillez m'excuser pour mon retard. Je _(faire)_ tout pour que cela ne se reproduise pas. 

    * [ ] ferai
    * [ ] ferais
    * [ ] ferait
    ??? done "correction"

        :heavy_check_mark: ferai

=== ":two:"
    Je _(souhaiter)_ connaître les missions que vous êtes en mesure de me proposer.  

    * [ ] souhaiterai
    * [ ] souhaiterais
    * [ ] souhaiterait
    ??? done "correction"

        :heavy_check_mark: souhaiterais

=== ":three:"
    Vous me demandez une attestation de logement. Je vous l'_(envoyer)_ dès que possible.
  

    * [ ] enverrai
    * [ ] enverrais
    * [ ] enverrait
    ??? done "correction"

        :heavy_check_mark: enverrai

=== ":four:"
    Je _(venir)_ bien vous remettre les documents en personne, mais je ne peux pas me déplacer pour le moment. 
  

    * [ ] viendrai
    * [ ] viendrais
    * [ ] viendrait
    ??? done "correction"

        :heavy_check_mark: viendrais

=== ":five:"
    Par ailleurs, je _(être)_ dans l'incapacité de me rendre chez M. Javier demain après-midi.  
  

    * [ ] serai
    * [ ] serais
    * [ ] serait
    ??? done "correction"

        :heavy_check_mark: serai

=== ":six:"
    Je vous _(être)_ reconnaissante de bien vouloir déclarer cet accident à la caisse d'assurance maladie.   
  

    * [ ] serai
    * [ ] serais
    * [ ] serait
    ??? done "correction"

        :heavy_check_mark: serais

=== ":seven:"
    Je vous _(communiquer)_ les dates de mon retour dès que le médecin aura validé ma reprise.    
  

    * [ ] communiquerai 
    * [ ] communiquerais
    * [ ] communiquerait
    ??? done "correction"

        :heavy_check_mark: communiquerai 

=== ":eight:"
    Je _(faire)_ une promenade avec Mme Gérard s'il ne pleut pas demain.     
  

    * [ ]  ferai
    * [ ]  ferais
    * [ ]  ferait
    ??? done "correction"

        :heavy_check_mark:  ferai

=== ":nine:"
    S'il le pouvait, il _(habiter)_ à Montpellier, mais il travaille à Paris.      
  

    * [ ] habiterai
    * [ ] habiterais
    * [ ] habiterait
    ??? done "correction"

        :heavy_check_mark: habiterait

=== ":keycap_ten:"
    Si la formation était en ligne, je _(pouvoir)_ me réveiller plus tard le matin
  

    * [ ] pourrai
    * [ ] pourrais
    * [ ] pourrait
    ??? done "correction"

        :heavy_check_mark: pourrais