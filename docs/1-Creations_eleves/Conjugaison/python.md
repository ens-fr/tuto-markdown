## Script
Voila un script python que j'ai écrit qui conjugue un verbe au futur.

!!! warning "Mise en garde"
    Ce script n'est pas fiable a 100%, il peut contenir des erreurs que vous pouvez signaler sur [ce lien](https://hackmd.io/7oaQ-qhFQeqQRpGFzpbLgA)

!!! info "Et pour le conditionnel ?"
    Il vous suffit de remplacer cette ligne
    ```python
    terminaisons = ["ai", "as", "a", "ons", "ez", "ont"]
    ```
    par celle là
    ```python
    terminaisons = ["ais", "ais", "ait", "ions", "iez", "aient"]
    ```

```python
    --8<--- "docs/scripts/conjugaison_futur.py"
```

## Executer le code

Vous pouvez copier/coller le code dans une [console basthon](https://console.basthon.fr)  
[![logo console basthon](https://basthon.fr/theme/assets/img/basthon-console.svg){width=150}](https://console.basthon.fr)  
[source](https://console.basthon.fr)

Ou télécharger le code:

- Compressé en [.zip](){ .md-button .md-button--primary }
- Compressé en [.tar.gz](){ .md-button .md-button--primary }
- Pas [compressé](scripts/conjugaison_futur.py){ .md-button .md-button--primary }
