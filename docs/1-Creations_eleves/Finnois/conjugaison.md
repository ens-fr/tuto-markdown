# La conjugaison en finnois

## Le finnois ? Complexe ? Non... pas du tout...

!!! faq "Comment fonctionne la conjugaison en finnois ?"
    Il faut le dire, la conjugaison en finnois est très complexe. Il y a certes peu d'exception dans la conjugaison, mais énormément de règles régissant et encadrant très précisément les différentes conjugaisons de la langue, tel le système d'harmonie vocalique et d'alternance consonantique mentionnée dans la partie sur la phonologie.  
    Dans cette partie, je vais vous détailler le fonctionnement de la conjugaison de la langue finnoise en essayant d'être le plus précis et clair possible.

!!! info "Groupes verbaux"
    On peut catégoriser les verbes en 6 groupes :
      
    -1^er^ groupe : les verbes finissant en <span style="color:#4BADE5">-aa, -ea, -eä, -ia, -iä, -oa, -ua, -yä, -ää, -öä</span>  
    -2^ème^ groupe : les verbes finissant en <span style="color:#4BADE5">-da/-dä</span>  
    -3^ème^ groupe : les verbes finissant en <span style="color:#4BADE5">-lla/-llä, -nna/-nnä, -rra/-rrä, -sta/-stä</span>  
    -4^ème^ groupe : les verbes finissant en <span style="color:#4BADE5">-ata/-ätä, -ota/-ötä, -uta/-ytä</span>  
    -5^ème^ groupe : les verbes finissant en <span style="color:#4BADE5">-ita/-itä</span>  
    -6^ème^ groupe : les verbes finissant en <span style="color:#4BADE5">-eta/-etä</span>  
    !!! warning "Les groupes 4,5 et 6"
        -Autre :

        Alors que le système des types de groupe finlandais a très peu d'exceptions, il existe des verbes des groupes 4, 5 et 6 qui passent d'un groupe à un autre. Ces verbes ne correspondent pas aux règles (simplifiées) utilisées dans la plupart des manuels de cours.  
          
        Principalement à cause de ce problème avec les groupe finlandais 4, 5 et 6, certains linguistes considèrent ces trois types de verbes comme un seul grand groupe de verbes se terminant par -Vta (voyelle + ta), qui comporte trois sous-groupes. De cette façon, ils évitent complètement la question de ces exceptions. Cependant, pour les apprenants de la langue finnoise, la combinaison de ces trois groupes n’est pas pratique.

!!! bug "Important"
    Je ne présenterai que les 4 temps de l'indicatif pour avoir les bases.

---

## Les temps de l'indicatif

!!! warning "Information"
    Avant de commencer, il faut savoir que le futur, n'existe pas. Cela ne veut pas dire qu'on ne peut pas l'exprimer. En effet, l'expression du futur est déterminé par le contexte, la déclinaison appliquée sur le mot qui suit ou par le biais d'un auxiliaire.

??? tip "Le présent"
    Le présent est le temps le plus facile en finnois.

    !!! Usage
        L'usage est le même qu'en français, il n'y a rien de spécial à dire.

    === "Premier groupe"
        
        Il suffit de remplacer le -a/-ä final par les terminaisons personnelles :

        | Présent | Puhua (parler) | Sanoa (dire) | Kysyä (demander) |
        | ------- | -------------- | ------------ | ---------------- |
        | Minä (je) | Puhun | Sanon | Kysyn |
        | Sinä (tu) | Puhut | Sanot | Kysyt |
        | Hän (il/elle) | Puhuu | Sanoo | Kysyy |
        | Me (nous) | Puhumme | Sanomme | Kysymme |
        | Te (vous) | Puhutte | Sanotte | Kysytte |
        | He (ils/elles) | Puhuvat | Sanovat | Kysyvät |



    === "Deuxième groupe"
        
        Il suffit de remplacer le -da/-dä par les terminaisons personnelles : (la 3PS ne prend pas d'allongement)

        | Présent | Saada (obtenir) | Juoda (boire) | Syödä (manger) |
        | ------- | -------------- | ------------ | ---------------- |
        | Minä (je) | Saan | Juon | Syön |
        | Sinä (tu) | Saat | Juot | Syöt |
        | Hän (il/elle) | Saa | Juo | Syö |
        | Me (nous) | Saamme | Juomme | Syömme |
        | Te (vous) | Saatte | Juotte | Syötte |
        | He (ils/elles) | Saavat | Juovat | Syövät |

    === "Troisième groupe"

        Il suffit de remplacer -la/-lä, -na/-nä, -ra/-rä,ou-ta/-tä. Et à cette racine, rajouter -e- suivi par les terminaisons personnelles.

        | Présent | Tulla (venir) | Mennä (aller) | Nousta (se tenir debout) |
        | ------- | -------------- | ------------ | ---------------- |
        | Minä (je) | Tulen | Menen | Nousen |
        | Sinä (tu) | Tulet | Menet | Nouset |
        | Hän (il/elle) | Tulee | Menee | Nousee |
        | Me (nous) | Tulemme | Menemme | Nousemme |
        | Te (vous) | Tulette | Menette | Nousette |
        | He (ils/elles) | Tulevat | Menevät | Nousevat |

    === "Quatrième groupe"

        Il suffit de remplacer le -ta/-tä par un -a- suivi des terminaisons personnelles.  
        Dans le cas où le verbe est en -ata/-ätä, alors on ne redouble pas la voyelle une nouvelle fois au 3PS pour éviter de se retrouver avec trois voyelle identiques qui se suivent.   

        | Présent | Haluta (vouloir) | Osata (être capable de) | Pakata (emballer) |
        | ------- | -------------- | ------------ | ---------------- |
        | Minä (je) | Haluan | Osaan | Pakkaan |
        | Sinä (tu) | Haluat | Osaat | Pakkaat |
        | Hän (il/elle) | Haluaa | Osaa | Pakkaa |
        | Me (nous) | Haluamme | Osaamme | Pakkaamme |
        | Te (vous) | Haluatte | Osaatte | Pakkaatte |
        | He (ils/elles) | Haluavat | Osaavat | Pakkaavat |

    === "Cinquième groupe"

        Il faut transformer le -ita/-itä en -itse- pui rajouter les terminaisons personnelles.

        | Présent | Häiritä (déranger) | Tarvita (avoir besoin) | Hallita (contrôler) |
        | ------- | -------------- | ------------ | ---------------- |
        | Minä (je) | Häiritsen | Tarvitsen | Hallitsen |
        | Sinä (tu) | Häiritset | Tarvitset | Hallitset |
        | Hän (il/elle) | Häiritsee | Tarvitsee | Hallitsee |
        | Me (nous) | Häiritsemme | Tarvitsemme | Hallitsemme |
        | Te (vous) | Häiritsette | Tarvitsette | Hallitsette |
        | He (ils/elles) | Häiritsevät | Tarvitsevat | Hallitsevat |

    === "Sixième groupe"

        Il faut ici remplacer le -ta/-tä par -ne- suivi des terminaisons personnelles.

        | Présent | Vanheta (vieillir) | Lämmetä (réchauffer) | Noureta (Rajeunir) |
        | ------- | -------------- | ------------ | ---------------- |
        | Minä (je) | Vanhenen | Lämpenen | Nuorenen |
        | Sinä (tu) | Vanhenet | Lämpenet | Nuorenet |
        | Hän (il/elle) | Vanhenee | Lämpenee | Nuorenee |
        | Me (nous) | Vanhenemme | Lämpenemme | Nuorenemme |
        | Te (vous) | Vanhenette | Lämpenette | Nuorenette |
        | He (ils/elles) | Vanhenevat | Lämpenevät | Nuorenevat |

    === "Autre"

        **Les verbes suivants ressemblent au groupe 5 (-ita / -itä) mais se conjuguent comme au groupe 4. Voir les mots barrés pour savoir comment ils auraient été conjugués si nous suivions les règles à la lettre.**    
          
        selvitä (devenir clair) - selviän ~~(pas selvitsen)~~  
        hävitä (perdre, disparaître) - häviän ~~(pas hävitsen)~~  
          
        **Ensuite, nous avons des verbes qui ressemblent au groupe 6 (-eta / -etä) mais qui se conjuguent comme le groupe 4.**  
          
        hävetä (avoir honte) - häpeän ~~(pas häpenen)~~  
        kiivetä (monter) - kiipeän ~~(pas kiipenen)~~  
        ruveta (commencer) - rupean ~~(pas rupenen)~~  
        todeta (établir) - totean ~~(not totenen)~~  
          
        **Enfin, voici quelques verbes qui ressemblent au groupe 4 mais qui se conjuguent au groupe 6.**
          
        hapata (acidifier) - happanee ~~(pas happaa)~~  
        loitota (détourner) - loittonee ~~(pas loittoaa)~~  
        helpota (être plus facile) - helpponee ~~(pas helppoaa)~~  
        parata (aller mieux) - paranee ~~(pas paraa)~~

        ??? warning "Verbes irréguliers"
            Il n'y a que 2 verbes irrégulier en finnois, ainsi qu'un verbe qui possède une légère irrégularité. Il s'agit de Nähdä (voir), Tehdä (faire) et Olla (être)  
            Nähdä et Tehdä se conjugue de la même manière :

            | Nähdä | Tehdä |
            | ----- | ----- |
            | Näen | Teen |
            | Näet | Teet |
            | Näkee | Tekee |
            | Näemme | Teemme |
            | Näette | Teette |
            | Näkevät | Tekevät |

            Le verbe Olla (être) quant à lui, est un verbe du troisième groupe régulier sauf à la troisième personne du singulier.

            | Olla |
            | ----- |
            | Olen |
            | Olet |
            | <span style="color:#F78E01">On</span> |
            | Olemme |
            | Olette |
            | Olevat |


    ??? danger "Alternance consonatique au présent"
        IMPORTANT : Au présent, certains verbes subissent une alternance consonnatique.
        
        | Groupe | Infinitif | Minä | Sinä | Hän | Me | Te | He |
        | ------ | --------- | ---- | ---- | --- | -- | -- | -- |
        | Groupe 1 | <span style="color:#F78E01">Fort</span> | <span style="color:#38CB98">Faible</span> | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#38CB98">Faible</span> | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> |
        | Groupe 2 | - | - | - | - | - | - | - |
        | Groupe 3 | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> |
        | Groupe 4 | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> |
        | Groupe 5 | - | - | - | - | - | - | - |
        | Groupe 6 | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> |

??? tip "L'imparfait"
    Le temps imparfait est assez compliqué pour certains groupes, et assez simple pour d'autres. Dans l'ensemble, il y a beaucoup de choses à prendre en considération : la gradation des consonnes, les types de verbes et les lettres dans lesquelles les verbes se terminent. Chacun de ceux-ci a un effet sur le verbe au temps imparfait.  
    Le marqueur de l'imparfait est -i-, mais peut également apparaître sous la forme -si- et -oi-.
      
    !!! Usage
        L'imparfait est un des 3 temps du passé de la langue finnoise. L'usage diffère du français. On va s'en servir pour des évènements passés dans le cas de phrase où la période de l'action est défini par des mots tel que "Eilen" (hier), "Viimme viikolla" (durant la semaine dernière)...
        !!! example "Exemple"
            Sö**<span style="color:#EC1865">in</span>** eilen puuroa. => J'ai mangé du porridge hier.  
            He istu**<span style="color:#EC1865">ivat</span>** penkillä väsyneinä. => Ils s'assirent fatigués sur le banc.  
            Tarvits**<span style="color:#EC1865">imme</span>** apua. =>	Nous avions besoin d'aide.  

    ??? tip "Sous-groupes du groupe 1"
        === "1^er^ Sous-groupe"
            Pour les verbes qui ont un infinitif qui se termine par -ua, -yä, -oa ou -öä, il faut des -i- derrière les u, y, o et ö sans apporter de modifications à la tige. Il faut noter que la troisième personne du singulier se termine par un seul -i.

            | Imparfait | Sanoa (dire) |
            | --------- | ------------ |
            | Minä | Sano<u>in</u> |
            | Sinä | Sano<u>it</u> |
            | Hän | Sano<u>i</u> |
            | Me | Sano<u>imme</u> |
            | Te | Sano<u>itte</u> |
            | He | Sano<u>ivat</u> |

        === "2^nd^ Sous-groupe"
            Pour les verbes qui se terminent par -ea, -eä ou -ää dans leur forme de base (infinitif), il faut supprimer le -e- ou -ä- lors de l'ajout du -i- de l'imparfait.

            | Imparfait | Itkeä (pleurer) |
            | --------- | ------------ |
            | Minä | Itk<u>in</u> |
            | Sinä | Itk<u>it</u> |
            | Hän | Itk<u>i</u> |
            | Me | Itk<u>imme</u> |
            | Te | Itk<u>itte</u> |
            | He | Itk<u>ivät</u> |

        === "3^ème^ Sous-groupe"
            Pour les verbes dont la forme de base se termine par -ia / iä, le présent et le passé semblent identiques dans la plupart des formes. Par exemple, pour le verbe "tanssia", le présent et le passé sont "minä tanssin" : «Je danse» ET «J'ai dansé».  
              
            C’est le cas parce que vous supprimez d’abord le -i- (tanss-) de la tige, puis ajoutez le -i- de l’imparfait. En tant que tel, vous vous retrouvez avec le même nombre de i aux deux temps. La seule personne où il y a une différence est la troisième personne: le présent est "hän tanssii" et le passé "hän tanssi".

            | Imparfait | Tanssia (danser) |
            | --------- | ------------ |
            | Minä | Tanss<u>in</u> |
            | Sinä | Tanss<u>it</u> |
            | Hän | Tanss<u>i</u> |
            | Me | Tanss<u>imme</u> |
            | Te | Tanss<u>itte</u> |
            | He | Tanss<u>ivat</u> |

        === "4^ème^ Sous-groupe"
            Cette catégorie est appelée verbes "a… a → a… oi" parce que ce sont des mots de deux syllabes qui ont un -a- dans la première et la dernière syllabe. Ce second -a- se transformera en -oi- à l'imparfait.

            | Imparfait | Antaa (donner) |
            | --------- | ------------ |
            | Minä | **<span style="color:#118BC8">A</span>**nn<u>oin</u> |
            | Sinä | **<span style="color:#118BC8">A</span>**nn<u>oit</u> |
            | Hän | **<span style="color:#118BC8">A</span>**nn<u>oi</u> |
            | Me | **<span style="color:#118BC8">A</span>**nn<u>oimme</u> |
            | Te | **<span style="color:#118BC8">A</span>**nn<u>oitte</u> |
            | He | **<span style="color:#118BC8">A</span>**nn<u>oivat</u> |

        === "5^ème^ Sous-groupe"
            Pour tous les verbes se terminant par -aa à l'exception de ceux du 4eme sous-groupe, le -i- remplacera la voyelle finale.

            | Imparfait | Ostaa (acheter) |
            | --------- | ------------ |
            | Minä | Ost<u>in</u> |
            | Sinä | Ost<u>it</u> |
            | Hän | Ost<u>i</u> |
            | Me | Ost<u>imme</u> |
            | Te | Ost<u>itte</u> |
            | He | Ost<u>ivat</u> |
        
        === "6^ème^ Sous-groupe"
            Le dernier sous-groupe du groupe 1 est assez rare et concerne les verbes finissant en -ltaa/-ltää, -rtaa/-rtää, -ntaa/-ntää, –vvtaa/-vvtää, mais comprend quelques verbes très fréquemment utilisés (par exemple, tietää (-vvtää) et ymmärtää (-rtää) en font partie).

            | Imparfait | Tietää (savoir) |
            | --------- | ------------ |
            | Minä | Tie<u>sin</u> |
            | Sinä | Tie<u>sit</u> |
            | Hän | Tie<u>si</u> |
            | Me | Tie<u>simme</u> |
            | Te | Tie<u>sitte</u> |
            | He | Tie<u>sivät</u> |

            | Imparfait | Ymmärtää (savoir) |
            | --------- | ------------ |
            | Minä | Ymmär<u>sin</u> |
            | Sinä | Ymmär<u>sit</u> |
            | Hän | Ymmär<u>si</u> |
            | Me | Ymmär<u>simme</u> |
            | Te | Ymmär<u>sitte</u> |
            | He | Ymmär<u>sivät</u> |

    ??? tip "Sous-groupes du groupe 2"

        Le deuxième groupe a trois sous-groupes. Il a également un verbe irrégulier: le verbe käydä (minä kävin, sinä kävit, hän kävi...).

        === "1^er^ Sous-groupe"
            Les verbes de type "Myydä" sont courts: ils ont deux syllabes. Dans la première syllabe, ils peuvent avoir une voyelle longue ou une diphtongue.  
            Dans le cas d'une voyelle longue, il faut supprimer l’une des voyelles longues lorsque vous ajoutez le -i de l’imparfait.

            | Imparfait | Myydä (acheter) |
            | --------- | ------------ |
            | Minä | My<u>in</u> |
            | Sinä | My<u>it</u> |
            | Hän | My<u>i</u> |
            | Me | My<u>imme</u> |
            | Te | My<u>itte</u> |
            | He | My<u>ivät</u> |

            Pour les verbes qui ont une diphtongue dans la première syllabe (vie-dä, juo-da, syö-da), on dois supprimer la première voyelle de la première syllabe. Après cela, on ajoute le -i- imparfait à la fin de la tige.

            | Imparfait | Syödä (manger) |
            | --------- | ------------ |
            | Minä | Sö<u>in</u> |
            | Sinä | Sö<u>it</u> |
            | Hän | Sö<u>i</u> |
            | Me | Sö<u>imme</u> |
            | Te | Sö<u>itte</u> |
            | He | Sö<u>ivät</u> |

            | Imparfait | Juoda (boire) |
            | --------- | ------------ |
            | Minä | Jo<u>in</u> |
            | Sinä | Jo<u>it</u> |
            | Hän | Jo<u>i</u> |
            | Me | Jo<u>imme</u> |
            | Te | Jo<u>itte</u> |
            | He | Jo<u>ivat</u> |

        === "2^ème^ Sous-groupe"
            Le modèle ci-dessus n'est pas vrai pour les verbes du groupe 2 qui se terminent par -ida / -idä. Tous les verbes du groupe 2 qui se terminent par -ida ont exactement la même apparence lorsqu'ils sont conjugués au présent et au passé. Ceci est dû à la même raison que pour les verbes de type "Tanssia" (danser): lors de la conjugaison de ces verbes, vous remplacez le –i– du radical du verbe par le –i– de l'imparfait.  
              
            Ce groupe comprend à la fois des verbes courts et plus longs: des verbes comme "Uida" de deux syllabes qui se terminent par -ida et des verbes comme "Tupakoida" plus longs (3 syllabes et plus). On pourrait dire que ces verbes sont les plus simples, car leur conjugaison au présent et au passé semble identique.

            | Imparfait | Voida (pouvoir) |
            | --------- | ------------ |
            | Minä | Vo<u>in</u> |
            | Sinä | Vo<u>it</u> |
            | Hän | Vo<u>i</u> |
            | Me | Vo<u>imme</u> |
            | Te | Vo<u>itte</u> |
            | He | Vo<u>ivat</u> |

            | Imparfait | Tupakoida (fumer) |
            | --------- | ------------ |
            | Minä | Tupako<u>in</u> |
            | Sinä | Tupako<u>it</u> |
            | Hän | Tupako<u>i</u> |
            | Me | Tupako<u>imme</u> |
            | Te | Tupako<u>itte</u> |
            | He | Tupako<u>ivat</u> |

        === "3^ème^ Sous-groupe"
            En plus des verbes nähdä (voir) et tehdä faire, le temps imparfait a un verbe irrégulié: käydä (visiter un lieu).

            | Présent | Nähdä | Tehdä | Käydä |
            | ------- | -------------- | ------------ | ---------------- |
            | Minä | Nä<u>in</u> | Te<u>in</u> | Käv<u>in</u> |
            | Sinä | Nä<u>it</u> | Te<u>it</u> | Käv<u>it</u> |
            | Hän | Näk<u>i</u> | Tek<u>i</u> | Käv<u>i</u> |
            | Me | Nä<u>imme</u> | Te<u>imme</u> | Käv<u>imme</u> |
            | Te | Nä<u>itte</u> | Te<u>itte</u> | Käv<u>itte</u> |
            | He | Näk<u>ivät</u> | Tek<u>ivät</u> | Käv<u>ivät</u> |

    ??? tip "Les groupes 3, 4, 5 et 6"
        C'est groupe-là eux non pas de sous groupe (oufff....)
        === "3^ème^ groupe"
            Dans le 3eme groupe, les verbes ont tous en commun qu'il y aura un -e- à la fin du radical. Ce -e- sera remplacé par un -i- dans l'imparfait. Pour la troisième personne, les deux -e- disparaîtront.

            | Imparfait | Nousta (se tenir debout) |
            | --------- | ------------ |
            | Minä | Nous<u>in</u> |
            | Sinä | Nous<u>it</u> |
            | Hän | Nous<u>i</u> |
            | Me | Nous<u>imme</u> |
            | Te | Nous<u>itte</u> |
            | He | Nous<u>ivat</u> |
        
        === "4^ème^ groupe"
            On remplace le -ta/tä du verbe et on le remplace par -si- suivi de la terminaison personnelle

            | Imparfait | Haluta (vouloir) |
            | --------- | ------------ |
            | Minä | Halu<u>sin</u> |
            | Sinä | Halu<u>sit</u> |
            | Hän | Halu<u>si</u> |
            | Me | Halu<u>simme</u> |
            | Te | Halu<u>sitte</u> |
            | He | Halu<u>sivat</u> |

        === "5^ème^ groupe"
            Le 5eme groupe ressemble beaucoup au groupe 3. Dans le sens que les deux ont un -e- à la fin de leur racine, et ce -e- est remplacé par un -i- à l'imparfait pour les deux groupes.

            | Imparfait | Tarvita (avoir besoin) |
            | --------- | ------------ |
            | Minä | Tarvits<u>in</u> |
            | Sinä | Tarvits<u>it</u> |
            | Hän | Tarvits<u>i</u> |
            | Me | Tarvits<u>imme</u> |
            | Te | Tarvits<u>itte</u> |
            | He | Tarvits<u>ivat</u> |

        === "6^ème^ groupe"
            Comme pour les groupes 3 et 5, il suffit de changer le -e- final en -i-.

            | Imparfait | Tarvita (avoir besoin) |
            | --------- | ------------ |
            | Minä | Vanhen<u>in</u> |
            | Sinä | Vanhen<u>it</u> |
            | Hän | Vanhen<u>i</u> |
            | Me | Vanhen<u>imme</u> |
            | Te | Vanhen<u>itte</u> |
            | He | Vanhen<u>ivat</u> |
            
    ??? danger "Alternance consonatique à l'imparfait"
        IMPORTANT : À l'imparfait, certains verbes subissent une alternance consonnatique. Il s'agit de la même alternance qu'au présent.
        
        | Groupe | Infinitif | Minä | Sinä | Hän | Me | Te | He |
        | ------ | --------- | ---- | ---- | --- | -- | -- | -- |
        | Groupe 1 | <span style="color:#F78E01">Fort</span> | <span style="color:#38CB98">Faible</span> | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#38CB98">Faible</span> | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> |
        | Groupe 2 | - | - | - | - | - | - | - |
        | Groupe 3 | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> |
        | Groupe 4 | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> |
        | Groupe 5 | - | - | - | - | - | - | - |
        | Groupe 6 | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> |

??? tip "Le parfait"
    Le parfait est en finnois un temps composé. Il se forme avec le verbe "Être" au présent suivi du verbe au participe passé actif, que nous auront l'occasion de voir plus tard.
    !!! note "Usage du temps Parfait"
        Lorsque vous regardez quand utiliser le temps parfait, vous devez le faire par rapport au temps imparfait. Les deux temps sont utilisés pour des choses dans le passé, mais signifient quelque chose de différent. La façon dont ils sont utilisés en français n'est pas toujours comme en finnois.
          
        __1.1) Quand quelque chose se passe encore__  
          
        Premièrement, le temps parfait est utilisé pour des choses qui ont commencé dans le passé, mais qui se poursuivent encore au moment de parler.  
        !!! example "exemple"
            **<span style="color:#8E9190">Exemple 1 :</span>**  
              
	        - <span style="color:#FF3600">Parfait:</span>  
              
            <span style="color:#FF3600">Olen asunut</span> Suomessa yhden vuoden.  
            "Je vis en Finlande depuis un an." *(Je vis toujours en Finlande)*  
              
	        - <span style="color:#37A500">Imparfait:</span>  
              
            <span style="color:#37A500">Asuin</span> Suomessa yhden vuoden.  
            "J'ai vécu en Finlande pendant un an." *(À l'heure actuelle, je ne vis pas en Finlande, mais j'y ai vécu pendant un an.)*  
              
            **<span style="color:#8E9190">Exemple 2 :</span>**  
              
	        - <span style="color:#FF3600">Parfait:</span>  
              
		    <span style="color:#FF3600">Olen lukenut</span> tätä kirjaa monta tuntia.  
	        "Je lis ce livre depuis de nombreuses heures." *(Je vais toujours continuer.)*  
              
	        - <span style="color:#37A500">Imparfait:</span>  
              
		    <span style="color:#37A500">Luin</span> tätä kirjaa monta tuntia.  
	        "J'ai lu ce livre pendant de nombreuses heures." *(Je ne vais plus continuer.)*

        __1.2) Quand quelque chose est pertinent pour le moment actuel__  
          
        Lorsque quelque chose est fait, mais que le résultat est pertinent pour le moment actuel, vous utilisez également le temps parfait.  
        !!! example "exemple"
            - Nyt <span style="color:#FF3600">olen syönyt</span> tarpeeksi.  
	        "Maintenant, j'ai assez mangé."  
	        *(J'ai fini de manger, mais le résultat (que j'ai fait maintenant) est pertinent pour le moment.)*  
              
            - Kuka <span style="color:#FF3600">on kirjoittanut</span> Työmiehen vaimon?  
	        "Qui a écrit le livre‘ työmiehen vaimo ’?"  
	        *(Le livre a été écrit il y a longtemps mais les informations sont pertinentes en ce moment.)*  

        __1.3) Les phrases avec "Oletko Koskaan"__  
          
        Lorsque vous parlez de votre vie passée et de ce que vous avez fait ou non, vous utilisez le temps parfait en finnois.  
              
        Dans les phrases négatives, cela signifie que vous n’avez pas fait l’action, mais qu'on pourrai le faire plus tard. Par exemple, la phrase __«En ole käynyt Espanjassa»__ (je ne suis pas allé en Espagne) n’exclut pas la possibilité d’y aller plus tard.  
              
        Une question demandant si quelqu'un a déjà fait quelque chose dans le passé commence toujours par «oletko koskaan». La réponse directe n’aura pas non plus d’heure précise mentionnée (par exemple : __Kyllä, olen käynyt USA:ssa__ «Oui, j’ai été aux États-Unis.»). Lorsqu'on spécifie précisément le moment de l'événement, on utilise le temps imparfait à la place (par exemple: __Kävin USA:ssa viime vuonna__ «J'étais aux États-Unis l'année dernière»).  
        !!! example "exemple"
            <span style="color:#56AFBF">Oletko koskaan</span> matkustanut Thaimaahan? *(As-tu déjà voyagé en Thaïlande?)*  
            -En ole, mutta haluaisin kyllä! *(Non, mais je voudrais bien !)*  
              
            <span style="color:#56AFBF">Oletteko koskaan</span> syönyt thaimaalaista ruokaa? *(As-tu déjà mangé de la nourriture Thaï?)*  
            -Kyllä, olemme syönyt thaimaalaista ruokaa *(Oui, J'ai mangé de la nourriture Thaï.)*  
            -Söimme thaimaalaista ruokaa viime viikolla *(J'ai mangé de la nourriture Thaï la semaine dernière)*

    


??? tip "Le plus-que-Parfait"
    Comme le parfait, il s'agit d'un temps composé. Il se forme avec le verbe "Être" à l'imparfait suivi du verbe au participe passé actif.  

    !!! note "Usage"
        Le Plus-que-parfait exprime un événement antérieur à un autre événement, les deux événements ayant lieu dans le passé. Pour cette raison, la manière la plus courante d'utiliser le Plus-que-parfait est de le combiner avec l'imparfait. Son usage est très similaire au francais.  
        Dans les phrases avec une imparfait + plus-que-parfait, l'imparfait raconte quelque chose qui s'est passé dans le passé. Ensuite, le Plus-que-parfait vous informe d'un événement qui a eu lieu avant ce moment.

        !!! example "Exemple"
            **<span style="color:#8E9190">Exemple 1</span>**  
              
            Maija <span style="color:#37A500">lähti</span> lenkille, kun <span style="color:#845BE3">oli tehnyt</span> läksynsä.  
            "Maija est allée faire du jogging, quand elle avait fait ses devoirs"  
            = L'imparfait *"lähti"* (est parti) s'est produit dans le passé, et le plus-que-parfait *"oli tehnyt"* (avait fait) est arrivé avant cela.  
              
            **<span style="color:#8E9190">Exemple 2</span>**  
              
            <span style="color:#845BE3">Olin asunut</span> Suomessa kolme viikkoa, kun <span style="color:#37A500">aloitin</span> suomen kielen opiskelun.  
            «J'avais vécu en Finlande pendant trois semaines lorsque j'ai commencé mon apprentissage du finnois.  
            = L'imparfait *"aloitin"* (j'ai commencé) s'est produit dans le passé, et le plus-que-parfait *"olin asunut"* (j'avais habité) est arrivé avant cela.  

## La négation

!!! info "La négation en finnois : Différent, mais efficace"
    En finnois, la construction de phrase négative est assez particulière. En effet, il ne s'agit pas simplement de rajouter un petit mot avant ou après le verbe comme en anglais ou en français ; en fait, il s'agit d'une conjugaison à part entière. Il ne faut pas en avoir peur, c'est assez intuitif en vérité, surtout pour le présent.

!!! note "L'exemple de la négation au présent"
    Pour former le négatif en finnois, c'est très facile à retenir et à faire. La négation se divise en 2 parties : l'"auxiliaire négatif" (on va en parler) suivi du verbe à la forme négative.  
    Pour donner une idée de la négation, voici un tableau qui compare la forme affirmative et négative du verbe "Tulla" (venir)

    | Présent | Affirmatif | Négatif |
    | ------- | ---------- | ------- |
    | Minä | Tulen | En tule |
    | Sinä | Tulet | Et tule |
    | Hän | Tulee | Ei tule |
    | Me | Tulemme | Emme tule |
    | Te | Tulette | Ette tule |
    | He | Tulevat | Eivät tule |

    Si on regarde bien, on observe que l'auxiliaire de négation change à toutes les personnes, il s'agit des six formes de "Ei" qui signifie "Non", qui changera selon la personne. Et on peut constater que le verbe "Tulla" pert sa terminaison personnelle. C'est tout. La formation du négatif se forme avec l'auxiliaire négatif correspondant à la personne désirée suivi du verbe conjugué à qui on a retiré sa terminaison personnelle.
    Cette règle s'appliquera de la même manière sur tout les verbes.
    !!! example "Application"
        - Je <span style="color:#04C294">vais</span> à l'école avec toi => Menen kouluun sinun kanssa  <span style="color:#04C294">(Affirmatif)</span>
        - Je <span style="color:#C21004">ne vais pas</span> à l'école en bus => En mene kouluun buusilla <span style="color:#C21004">(Négatif)</span>
          
        - Nous <span style="color:#04C294">mangerons</span> à la maison vendredi mais ma soeur <span style="color:#C21004">ne sera pas</span> là. => <span style="color:#04C294">Syömme</span> kotiin perjuntaina mutta siskoni <span style="color:#C21004">ei ole</span> siellä. (usage mixte)

## Poser une question en finnois

!!! info "La question"
    Pour formuler une question en finnois, on peut dire que c'est plutôt similaire au français malgré une petite différence notable.  

!!! tip "La question fermée"
    La formulation d'une question fermée (soit "est-ce que...) est assez facile, car similaire au français, il suffit de faire l'inversion sujet-verbe MAIS il faut aussi rajouter à la fin du verbe le suffixe **-ko/-kö** qui exprime à lui seul le "est-ce que". On a pu l'apercevoir dans l'expression de "Oletko koskaan" dans la partie sur le Parfait.
    !!! example "Application"
        - Peux-tu me donner l'eau, s'il te plaît ? => Voit<u>ko</u> sä antaa minulle vettä, ole kiltti ?  
        - Est-ce qu'ils ont besoin d'aide ? => Tarvitsevat<u>ko</u> he apua ?
!!! tip "La question ouverte"
    La formulation des questions ouvertes est similaire au français de part l'usage des pronoms interrogatifs (Quoi, qui, où...). La particule **-ko/-kö** ne s'utilise pas ici.
    !!! example "Application"
        - Qui êtes-vous ? => Kuka te olette ?  
        - Väinö ! Où es-tu ?! => Väinö ! Missä olet ?!  
        - Quelle genre de voiture est-ce ? => Millainen auto on ?
        - Où vont les enfants ? => Mihin lapset menevät ?