# Le Métal



## Qu'est-ce que c'est ?

Le Métal (ou Heavy metal pour les plus anciens) est un [genre musical](https://fr.wikipedia.org/wiki/Genre_musical) descendant du [Hard Rock](https://fr.wikipedia.org/wiki/Hard_rock).


## Les grandes figures du Métal

Les précurseurs du Métal ont souvent été désignés de satanistes ou de fous.
Il faut dire que Ozzy Osbourne n'a pas aidé, du moins au niveau de son style.

![Ozzy Osbourne](https://www.lacote.ch/media/image/94/normal_16_9/108471176_highres.jpg)

Bien sur Black Sabbath, dont il est le chanteur, a grandement révolutionné le style avec l'aide d'autres groupes comme :

- Mötley Crüe
- Led Zepplin
- Deep Purple
- Alice Cooper

Par la suite d'autre groupes comme :

- Metallica
- Iron Maiden
- Def Leppard
- Motörhead

Ont émergés et ont à leur tour rendu le style plus extrème.

## Le Métal moderne

De nos jours le Métal est très dévalorisé car le style est devenu plus technique plus violent mais toujours écoutable car aujourd'hui encore beaucoup de personnes pensent que ce n'est que du bruit.

Voilà quelques exemples pas violents :

- Architects

<iframe width="320" height="220" src="https://www.youtube.com/embed/OUSJp8ohSAU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Bring Me The Horizon

<iframe width="320" height="220" src="https://www.youtube.com/embed/rju0pbdm4z4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Et quelques exemples plus violents :

- Breakdown Of Sanity

<iframe width="320" height="220" src="https://www.youtube.com/embed/FyHfn9px0Ak" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Lorna Shore

<iframe width="320" height="220" src="https://www.youtube.com/embed/HbfnfWnh7SM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Dans le Métal il y en a pour tout les goûts du violent, du calme, du lent, du rapide, même des musiques connues dans certains cas.
C'est pourquoi ce style devrait être moins dévalorisé et jugé à sa juste valeur.

## L'influence du Métal chez les jeunes

Le Métal chez les jeunes (sauf certains cas) c'est fini mais dans les années 80 c'est avec des groupes comme The Offspring, Sum 41, Blink 182 ou encore Green Day que les jeunes allaient au skate park avec leurs potes pour sécher les cours et faire la fête.

Mais ce style a rapidement été dépassé par le Nu Métal (ou Néo Métal) avec des groupes comme Linkin Park, Limp Bizkit ou Slipknot qui eux même ont été dépassés par le Rap.

Et aujourd'hui c'est toujours le Rap qui domine en terme de style musical chez les jeunes.