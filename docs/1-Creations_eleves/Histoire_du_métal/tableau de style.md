# Les Styles

Voici un tableau avec plusieurs style de Métal et leurs artistes correspondants :

| Style | Groupes | Chansons |
|-------|---------|----------|
| Metalcore | All That Remains | [Forever In Your Hand](https://www.youtube.com/watch?v=5cd0ROfZtjU) |
| Pirate Metal | Alestorm | [Scraping The Barrel](https://www.youtube.com/watch?v=TQ2Kg7P0o7I) |
| Metalcore | A Day To Remember | [Degenerates](https://www.youtube.com/watch?v=mq3iRf8QA28) |
| Metalcore | Bring Me The Horizon | [Can You Feel My Heart](https://www.youtube.com/watch?v=QJJYpsA5tv8) |
| Heavy Metal | Black Sabbath | [Iron Man](https://www.youtube.com/watch?v=5s7_WbiR79E) |
| Brutal Death Metal | Cannibal Corpse | [I Cum Blood](https://www.youtube.com/watch?v=WBAM1W3P9II) |
| Death Mélodique | Children Of Bodom | [Downfall](https://www.youtube.com/watch?v=MRK3Qx4lNPo) |
| Hard Rock | Deep Purple | [Smoke On The Water](https://www.youtube.com/watch?v=zUwEIt9ez7M) |
| Power Metal | Dragonforce | [Through The Fire And Flames](https://www.youtube.com/watch?v=0jgrCKhxE1s) |
| Heavy Metal | Edguy | [The Spirit](https://www.youtube.com/watch?v=dZFs9UeN9vs) |
| Nu Metal | Evanescence | [Bring Me To Life](https://www.youtube.com/watch?v=3YxaaGgTQYM) |
| Metal Progressif | Freak Kitchen | [Vaseline Bizniz](https://www.youtube.com/watch?v=S8yb8gVBJQA) |
| Rock Alternatif | Faith No More | [Epic](https://www.youtube.com/watch?v=ZG_k5CSYKhg) |
| Death Metal | Gojira | [Silvera](https://www.youtube.com/watch?v=iVvXB-Vwnco) |
| Power Metal | Helloween | [I Want Out](https://www.youtube.com/watch?v=FjV8SHjHvHk) |
| Heavy Metal | Iron Maiden | [The Trooper](https://www.youtube.com/watch?v=X4bgXH3sJ2Q) |
| Death Mélodique | In Flames | [Only For The Weak](https://www.youtube.com/watch?v=qnLunQEcMn0) |
| Heavy Metal | Judas Priest | [Breaking The Law](https://www.youtube.com/watch?v=BXtPycm5dGc) |
| Nu Metal | KoЯn | [Freak On A Leash](https://www.youtube.com/watch?v=jRGrNDV2mKc) |
| Metalcore | Killswitch Engage | [The End Of Heartache](https://www.youtube.com/watch?v=h4QGc06KnBE) |
| Nu Metal | Linkin Park | [In The End](https://www.youtube.com/watch?v=eVTXPUF4Oz4) |
| Hard Rock | Lordi | [Hard Rock Hallelujah](https://www.youtube.com/watch?v=lYwHhQiMGdE) |
| Metal Industriel | Marilyn Manson | [Sweet Dreams](https://www.youtube.com/watch?v=QUvVdTlA23w) |
| Thrash Metal | Metallica | [Nothing Else Matters](https://www.youtube.com/watch?v=tAGnKpE4NCI) |
| Metal Symphonique | Nightwish | [Wish I Had An Angel](https://www.youtube.com/watch?v=wEERFBI9eCg) |
| Heavy Metal | Ozzy Osbourne | [Crazy Train](https://www.youtube.com/watch?v=hQ_Z-10dXSE) |
| Nu Metal | Papa Roach | [Last Resort](https://www.youtube.com/watch?v=Hm7vnOC4hoY) |
| Punk Rock | Rise Against | [Under The Knife](https://www.youtube.com/watch?v=bYOkOE8REXU) |
| Hard Rock | Scorpions | [Still Loving You](https://www.youtube.com/watch?v=7pOr3dBFAeY) |
| Nu Metal | Slipknot | [Psychosotial](https://www.youtube.com/watch?v=5abamRO41fE) |
| Metal Alternatif | Three Days Grace | [I Hate Everything About You](https://www.youtube.com/watch?v=d8ekz_CSBVg) |
| Metal Parodique | Ultra Vomit | [Evier Metal](https://www.youtube.com/watch?v=c1kNwDfBrLY) |
| Hard Rock | Van Halen | [Jump](https://www.youtube.com/watch?v=SwYN7mTi6HM) |
| Hard Rock | Whitesnake | [Still Of The Night](https://www.youtube.com/watch?v=swPt9HBRXuE) |
| Deathcore | Within Destruction | [Hate Me](https://www.youtube.com/watch?v=z5dDFG3mr0g) |
| Hard Rock | ZZ Top | [La Grange](https://www.youtube.com/watch?v=rG6b8gjMEkw) |