# Côté Code
---

Les adeptes de la programmation se posent peut être cette question ?

>Au niveau du code ? C'est comment ?

Car oui, un code qui marche c'est bien, mais un code qui *humainement lisible* c'est meilleur !

---

## Pourquoi ?
Eh bien malgrès les idées reçus...
Dans le monde de la programmation, on ne parle pas tous la même langue...

Certains font du 
[**Java**](https://fr.wikipedia.org/wiki/Java_(langage))
, d'autres du **[C](https://fr.wikipedia.org/wiki/C_(langage))
/ [C++](https://fr.wikipedia.org/wiki/C%2B%2B)**
, du [**Python**](https://fr.wikipedia.org/wiki/Python_(langage))
, ou encore du [**Markdown**](https://fr.wikipedia.org/wiki/Markdown)
et de l'
[**HTML**](https://en.wikipedia.org/wiki/HTML).  

Tous sont soit *différents* ou *similaires*, même leurs utilisation sont variées,

Le *JAVA*, le *C* (plus spécialement le *C++*) et le *Python* sont plus **[orienté objet](https://fr.wikipedia.org/wiki/Programmation_orient%C3%A9e_objet)**.  
Le *Markdown* et le *HTML* eux sont des **[languages de balisage](https://fr.wikipedia.org/wiki/Langage_de_balisage)**  


Plus de contenue prochainement...
