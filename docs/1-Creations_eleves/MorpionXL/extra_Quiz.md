# Extra Quiz !
## Dans cette dernière partie, je vais poser plusieurs questions portant sur le jeu "Morpion XL".

---

=== "Question 1"
    ``` Python
        # Code
        >>> marque_joueur, petit_morpion = 'X', 0
        >>> morpion_XL = [ ["•" for _ in range (9) ] for _ in range (9) ]
        >>> choix_joueur = int(input())
        >>> morpion_XL[...][...] = ...
            
        # Entrée
        2
    ```

    Parmis les 4 propositions suivantes, laquelle permet d'obtenir :  
    `morpion_XL[0][2] = [•, •, 'X', •, •, •, •, •, •]`

    ---

    - [ ] Réponse 1:
        ``` Py
        >>> morpion_XL[petit_morpion][choix_joueur] = marque_joueur
        ```
    - [ ] Réponse 2:
        ``` Py
        >>> morpion_XL[petit_morpion][choix_joueur] = marque_joueur
        ```
    - [ ] Réponse 3:
        ``` Py
        >>> morpion_XL[choix_joueur][petit_morpion] = marque_joueur
        ```
    - [ ] Réponse 4:
        ``` Py
        >>> morpion_XL[choix_joueur][petit_morpion] = marque_joueur
        ```

    ---

    ??? "Correction"

        - :x: Réponse 1:
            ``` Py
            >>> morpion_XL[petit_morpion][choix_joueur] = marque_joueur
            ```
        - :heavy_check_mark: Réponse 2:
            ``` Py
            >>> morpion_XL[petit_morpion][choix_joueur] = marque_joueur
            ```
        - :x: Réponse 3:
            ``` Py
            >>> morpion_XL[choix_joueur][petit_morpion] = marque_joueur
            ```
        - :x: Réponse 4:
            ``` Py
            >>> morpion_XL[choix_joueur][petit_morpion] = marque_joueur
            ```

        !!! Explication 
            morpion_XL < petit_morpion < choix_joueur < marque_joueur = X

            La case n^o^*choix_joueur*, dans le *petit_morpion* qui est dans le *Morpion_XL* est maintenant égale à la *marque_joueur* (X).

=== "Question 2"
    ``` Python
        
    ```
    === "Réponses"
        - [ ] Réponse 1:
        - [ ] Réponse 2:
        - [ ] Réponse 3:
        - [ ] Réponse 4:
    ??? "Correction"
        !!!

