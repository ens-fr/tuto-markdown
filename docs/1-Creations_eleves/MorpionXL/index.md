# Le Morpion XL ? Jamais entendu !

> Moripion XL ? On dirait le jeu du morpion.  
> C'est presque ça, mais laissez moi
> vous expliquer tout en détail.

---

## Le Morpion

Le morpion es un jeu de réflexion tour par tour à 2 joueurs, vous devez choisir une case pour la marquer avec votre symbole et réussir à faire une ligne de 3 marques sur le plateau de jeu.  

>Petit exemple :  
>![animation du déroulement d'une partie de morpion](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Tic-tac-toe-game-1.svg/479px-Tic-tac-toe-game-1.svg.png)  
> Ici le joueur des X a gagné la partie.

## Le MorpionXL

Le MorpionXL est une variante, inventé par Dorian Guillaume.
(non déclaré publiquement, mais c'est son code qui est utilisé ici)
>![cadrillage vide du morpion](https://th.bing.com/th/id/OIP.avpscs3luekADFSyoirVvgAAAA?w=160&h=180&c=7&o=5&pid=1.7) ![cadrillage vide du morpion](https://th.bing.com/th/id/OIP.avpscs3luekADFSyoirVvgAAAA?w=160&h=180&c=7&o=5&pid=1.7)![cadrillage vide du morpion](https://th.bing.com/th/id/OIP.avpscs3luekADFSyoirVvgAAAA?w=160&h=180&c=7&o=5&pid=1.7)  
>![cadrillage vide du morpion](https://th.bing.com/th/id/OIP.avpscs3luekADFSyoirVvgAAAA?w=160&h=180&c=7&o=5&pid=1.7) ![cadrillage vide du morpion](https://th.bing.com/th/id/OIP.avpscs3luekADFSyoirVvgAAAA?w=160&h=180&c=7&o=5&pid=1.7) ![cadrillage vide du morpion](https://th.bing.com/th/id/OIP.avpscs3luekADFSyoirVvgAAAA?w=160&h=180&c=7&o=5&pid=1.7)  
>![cadrillage vide du morpion](https://th.bing.com/th/id/OIP.avpscs3luekADFSyoirVvgAAAA?w=160&h=180&c=7&o=5&pid=1.7) ![cadrillage vide du morpion](https://th.bing.com/th/id/OIP.avpscs3luekADFSyoirVvgAAAA?w=160&h=180&c=7&o=5&pid=1.7) ![cadrillage vide du morpion](https://th.bing.com/th/id/OIP.avpscs3luekADFSyoirVvgAAAA?w=160&h=180&c=7&o=5&pid=1.7)

## Les rêgles du jeu

*Au debut* de la partie *le premier morpion* dans lequel nous jouons *est celui du milieu*.  
Chaque case définit dans quel morpion, le joueur suivant devra jouer.

>Exemple :  
>Si *le joueur des O* joue dans *une case en haut à droite*, le *joueur des X* devra jouer dans *le morpion en haut à droite*.

>Ainsi de suite ...

## Comment gagné ?

Pour gagné, il faut évidement faire une ligne de 3 marques dans le **grand** morpion.

>Mais comment marque-t-on le grand morpion ?!  
>Si toute ses sont des morpions ? 
>Eh bien je vais te dire.

Pour ce faire, lorsque un **petit** morpion est remporté :  
- Tu peux toujour jouer dedans mais ne peux plus le gagné.  
- Au yeux du grand morpion, il est représenté par la marque de son vainqueur

C'est ce point cruciale qui va permettre aux joueurs de gagné ou de perdre.

