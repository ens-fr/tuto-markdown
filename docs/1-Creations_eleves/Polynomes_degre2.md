# Les fonctions polynomes du second degrés

---

## équation du second degré

!!! faq "qu'est ce que c'est qu'un polynome du second degré ?"
    On appelle _fonction polynome du second degré_ toute fonction qui s'écrit pour $x \in  \mathbb R$ :

    $$f(x) = ax^{2} + bx + c$$         
    
    !!! warning "$a$, $b$, $c$ sont des réel avec $a \neq 0$"

    Toutes solutions de l'équation $ax^{2} + bx + c = 0$ est appellées "racine" du polynome
    
    ??? info "voici un croquis de sa représentation graphique"
        ![graphique equation du second degré](images\graphique second degré.svg)

!!! faq "Qu'est ce qu'un _discriminant_ et comment le calcule t-on ?"
    Selon la valeur du discriminant noté $\Delta$ , nous pourront savoir le nombre de racines d'une quelconque équation du second degré
    
    $$ \Delta = b^{2} - 4\times a\times c$$

    ??? example "exemple"
        - Voici une équation du second degré : $2x^{2} + x + 5 = 0$

        - On définit les coefficients $a$, $b$ et $c$ du polynôme :
            - $a = 2$ , $b = 1$ , $c = 5$
        
        - Pour calculer le discriminant il faut appliquer sa formule $\Delta = b^{2} - 4\times a\times c$ 
        
        - Donc :   $\begin{align*} 
                       \Delta &= 1^{2} - 4\times 2\times 5 \\
                              &= 1 - 40 \\
                              &= -39
                    \end{align*}$

        - Le discriminant est égal à $-39$
    
    !!! info "Une fois le discriminant calculé, on peut savoir le nombre de solution du polynome :"

        === "si $\Delta < 0$"

            Il n'y a aucune solution pour l'equation du second degré.

            $$S\in \emptyset$$


        === "si $\Delta = 0$"


            Il y a une solution pour l'equation du second degré :

            - solution :  $x_{0} = \dfrac{-b}{2a}$

        === "si $\Delta > 0$" 

            Il y a deux solutions pour l'equation du second degré :

            - solution 1 :  $x_{1} = \dfrac{-b + \sqrt{\Delta}}{2a}$

            - solution 2 :  $x_{2} = \dfrac{-b - \sqrt{\Delta}}{2a}$

!!! tip "Coté Informatique 💻"


    - voici une petite fonction en python pour calculer un     discriminant
        ```Python
        def discriminant(a,b,c):
            delta = b**2 - 4*a*c
            return delta
        ```

    - Voici un scrip pour résoudre une équation du second degré :

        [script](https://console.basthon.fr/?script=eJyNk71u2zAQx3cBeoeDOlhSbDkxMgX11LVAgXovcKJomwBFyvwI9Eh1h059A71YjxTtxEYDVIMo3Mf_fncnin7QxkGP7phnefYJsB7regMP0NYjvRl4Bc4INf3pOXQeLGdaddDxg5nOTZ4N5HNlsdPCUSiH6Xzy6IRWFAISYa8NJb4Ajj-i6iy6hceimgt-U2C4coaiuYVXlNwbG5JxCS1wB6xJYVb7IwrH4eTfecksKJ8yLCjdt4ZOQpvO0kK5lxodcAWoDhKFrYIUZSniC7TsiNNvOrv4aZC56SeRlEz3RD1ob2LYcDRoeQRGYo-qpVCDp86DoajI19572ouH3XvYxRNwvqBkXobZdsIyI3qhMPYjXdBuwzpW8FxjPU_iO-_R0AgsTTVaYbvewjMymosAbsMeAH2YKM0jtPaKRmAreZ79S3Sm2IlUMQio6XygJe7zTOzL2fwZHquXPAN60sq_Lt52rRYIA8a1WS19sF32uxM07GUSH7h3MP0KbIO2gkqA9qCofXryjEvLU5F7Ii9nO2Ukou32DekDLIzbuxA1AekSPdIUVi2sodw0NVZ3MkA6eE2MBCGhWI4pMkGm5hJo-FNCg4P0lpaxuGm0mVPetfghdMf9eC1ub7GfCKMk8FW8s409GTfPo6pgXW5uWhk3KfjhP4KvfdOPda1NX2SMRan3pyUU4fYE3WJJR_UXnltCkQ){ .md-button }


---


## Inéquation du second degré

!!! faq "qu'est ce que c'est une inequation du second degré"
    Une _inéquation du second degré à une inconnue_ est une inéquation qui peut se mettre sous
    l’une des quatre formes suivantes : 

    - $ax^{2}+bx+c < 0$
    - $ax^{2}+bx+c\leq 0$
    - $ax^{2}+bx+c\geq 0$
    - $ax^{2}+bx+c > 0$

    avec $a \neq 0$

    ???+ tip "Raccourcis clavier"

        - On peut écrire $\leq$ avec le raccourci clavier ++alt+comma++
        - On peut écrire $\geq$ avec le raccourci clavier ++alt+period++

     

!!! info "resoudre une inequation du second degré"
    Pour resoudre une inequation du second degré il faut:

    1. Calculer le discriminant pour trouver les solution du polynome comme si il n'etait pas une inequation
    2. Repérer le signe de $a$ pour savoir quel signe mettre dans le tableau:
        - si $a$ est positif la representation graphique sera en forme $U$[^ip] 
        - si $a$ est negatif la representation graphique sera en forme $\cap$[^ad]
    2. Dresser un tableau des signes pour trouver les bonnes solution par rapport au signe de l'inequation

    [^ip]: La representation graphique sera une parabole, [voir des exemple](https://www.logamaths.fr/representation-graphique-dune-fonction-polynome-du-second-degre/)
    [^ad]: La representation graphique sera une parabole inversé

    !!! done "on peux discerner 3 cas"

        === "Cas 1"

            *Si $\Delta < 0$*

            ---

            - si $a < 0$ en noir
            - si $a > 0$ en rouge

        
            ![cas 1](images/cas 1.svg)


        === "Cas 2"

            *Si $\Delta = 0$*
        
            ---

            - si $a < 0$ en noir
            - si $a > 0$ en rouge

            ![cas 2](images/cas 2.svg)


        === "Cas 3"

            *Si $\Delta > 0$*
        
            ---

            - si $a < 0$ en noir
            - si $a > 0$ en rouge

            ![cas 3](images/cas 3.svg)

    Par la suite il faudra observer le signe de l'inequation ($<$,$>$,$\leq$,$\geq$) pour savoir quand est ce que $F(x)$ est positif ou négatif

    ??? example "exemple"
        - Voici une inequation $5x^{2}+8x+3 > 0$ nous chercherons ses solutions si elle en a.
        - Premièrement : calculer le discriminant   $\begin{align*} 
                       \Delta &= 8^{2} - 4\times 5\times 3 \\
                              &= 64 - 60 \\
                              &= 4
                    \end{align*}$
        - Puisque $\Delta > 0$ l'equation possède deux solution   $x_{1} = \dfrac{-b + \sqrt{\Delta}}{2a}$ et $x_{2} = \dfrac{-b - \sqrt{\Delta}}{2a}$
        - Apres calcul on trouve : $x_{1} = -1$ et $x_{2} = - \dfrac{3}{5}$
        - Puisque qu'on sait que $a$ est positif et que $\Delta > 0$ on peut donc établir un tableau de signe :
        ![Tableau des signes](images/exemple 1.svg)

        - Le signe de l'inquation est : "$>$" donc on cherche que quand $x$ est **strictement** superieur a $0$
            c'est a dire : $S \in ] - \infty ; -1 [  \cap  ] - \dfrac{3}{5} ; + \infty [$

??? tip "une petite video avec un autre exemple"
    <iframe width="560" height="315" src="https://www.youtube.com/embed/AEL4qKKNvp8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Les trois formes d'un poynome du second degré

!!! done " Il existe trois formes pour un polynome du second degré"

    === "Forme 1"

        _La forme polynominale_

        $$ax^{2}+bx+c$$

    === "Forme 2"

        _La forme factorisée_

        |Quand $\Delta > 0$|Quand $\Delta = 0$|Quand $\Delta < 0$|
        |------|-----|------|
        |$a(x-x_{1})(x-x_{2})$|$a(x-x_{0})$|On ne peut pas|

    === "Forme 3"

        _La forme canonique_

        $$ a(x- \alpha )^{2} + \beta $$

        !!! faq "comment calculer $\alpha$ ?"
            $\alpha = \dfrac{-b}{2a}$ 
        !!! faq "comment calculer $\beta$ ?"
            $\beta =  \begin{cases} f(\alpha)\\          \\ \dfrac{- \Delta}{4a}\end{cases}$  

        $S(\alpha ; \beta)$ est le sommet de la parrabolle


---

## Tableau de variation

!!! tip "Si $a > 0$"

    ![un tableau de variation](images/tbl1.svg)

!!! tip "si $a < 0$"

    ![un deuxième tableau de variation](images/tbl2.svg)






