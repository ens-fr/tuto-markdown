# Maths

![Maths](https://www.rts.ch/2020/08/11/16/11/9081328.image?w=1920&h=598)

### ^^Courte Définition des Mathématiques :^^
    
Les mathématiques sont la science qui étudie par le moyen du raisonnement déductif les propriétés d'êtres abstraits (nombres, figures géométriques, fonctions, espaces, etc.) ainsi que les relations qui s'établissent entre eux. 

### ^^Origine^^ 

Le mot « mathématiques » vient du grec μάθημα « mathêma » ou plutôt μαθήματα « mathêmata » qui est son pluriel et qui expliquerait peut être pourquoi aujourd'hui encore la discipline se désigne par son pluriel. Le mot « mathêma » signifiait le fait d'apprendre tout comme sa résultante : la connaissance et la science.

## ^^Equations du second degré^^
    
Equations du second degré
Définition :
Soit f une fonction polynôme de degré 2 de la forme :
f(x) = ax²+bx+c où a , b et c sont trois réels avec a ≠ 0.
Le nombre réel Δ, égal à b²−4ac est appelé le discriminant de f.
    
!!! donne "démonstration des valeurs de x"
    ax² + bx + c = 0  
    x² + 6/a * x + c/a = 0  
    x² + 6/a * x + b²/(4a²) - b²/(4a²) + c/a = 0  
    (x + b/2a)² - b²/(4*a²) + c/a = 0  
    (x + b/2a)² - b²/(4a²) + (4ac)/(4a²) = 0  
    (x + b/2a)² - (b² - 4ac) /(4a²) = 0  
    (x + b/2a) - √(b² - 4ac) /√(4a²) = 0  ou  x + b/2a) -(- (√(b² - 4ac))) /√(4a²)  
    (x + b/2a) - √(b² - 4ac) /(2a) = 0  ou  x + b/2a) -(- (√(b² - 4ac))) /√(2a)  
    (x + b/2a) - √(Δ) /(2a) = 0  ou  x + b/2a) -(- (√(Δ))) /√(2a)  
    (x + (b - √(Δ)) /(2a) = 0  ou  (x + (b + √(Δ)) /(2a) = 0  
    ==x = (-b + √(Δ))/(2a== ou  ==x = (-b - √(Δ))/(2a)==  



###  ^^Calculer Delta^^
    
!!! donne "Programme :"  
    ```python
    a = int(input()) 
    b = int(input())
    c = int(input())
    # Attention à ne pas se tromper dans le choix des nombres
    Δ = b**2-4*a*c
    print(Δ)
    ```


###  ^^Propriété :^^

=== "Si Δ < 0" 

    Alors l'équation f(x) = 0 n'admet ==aucune solution réelle==.
    f ne peut pas s'écrire sous forme factorisée.
        
    !!! donne "Exemple"
        Résoudre l'équation x² + 2x + 5 = 0  
    
        Solution :  
        a = 1 , b = 2 et c = 5.  
        Δ = b² - 4ac  
        Δ = 2² - 4×1×5  
        Δ = 4 - 20 = -16  
        Δ < 0, donc l'équation n'admet aucune solution réelle.

    
=== "Si Δ = 0"

    Alors l'équation f(x) = 0 admet une unique solution ==x0 = -b/2a==.
    La forme factorisée de f est ==f(x) = a(x−x0)²==.
        
    !!! donne "Exemple"
        Résoudre l'équation 9x² - 12x + 4 = 0
    
        Solution :  
        a = 9 , b = -12 et c = 4.  
        Δ = b² - 4ac  
        Δ = (-12)² - 4×9×4  
        Δ = 144-144 = 0  
        Δ = 0, donc l'équation admet 1 solution.  

        x0 = 122/2*9 = 12/18 = 2/3  
            
        L'équation 9x² - 12x + 4 = 0 admet une solution : ==2/3==   
        ==9x² - 12x + 4 = 9(x - 2/3)²==.
    
=== "Si Δ > 0" 
    
    Alors l'équation f(x) = 0 a deux solutions ==x1 = -b−Δ√2a== et ==x2 = -b+Δ√2a==.
    La forme factorisée de f est ==f(x) = a(x−x1)(x−x2)==.

    !!! donne "Exemple"
            
        Résoudre l'équation 2x² - 5x - 3 = 0
    
        Solution :  
        a = 2 , b = -5 et c = -3.  
        Δ = b² - 4ac  
        Δ = (-5)² - 4×2×(-3)  
        Δ = 25-(-24) = 25+24 = 49  
        Δ > 0, donc l'équation admet 2 solutions.  
            
        x1 = 5−√49/2×2  
        x1 = 5−7/4 = −2/4 = −1/2
            
        x2 = 5+√49/2×2  
        x2 = 5+7/4 = 12/4 = 3
            
        L'équation 2x² - 5x - 3 = 0 admet deux solutions : ==-1/2 et 3==.  
        ==2x² - 5x - 3 = 2(x+1/2)(x-3)==.
    
###   ^^Calculer les valeurs de x^^

!!! donne "Programme Python :"
    ```python
    a = int(input()) 
    b = int(input())
    c = int(input())
    # Attention à ne pas se tromper dans le choix des nombres
    Δ = b**2-4*a*c
     if Δ < 0 :
        print("Il n'existe aucune solution à l'équation, la foncion ne changera donc pas de signe.")
    
    if Δ == 0:
        x0 =  -b/(2*a)
        print("Le résultat de l'équation est", x0, "." )
    
    if Δ > 0 :
        x1 = (-b - sqrt(Δ))/(2*a)
        x2 = (-b + sqrt(Δ))/(2*a)
        print("Les deux possibilités de x sont", x1, "et", x2, ".")
    ```

Exemple de sortie à ce programme :

=== "Si Δ < 0"

    !!! donne "Exemple avec a = 1, b = 2, c = 5"  
        1  
        2  
        5  
        Il n'existe aucune solution à l'équation, la foncion ne changera donc pas de signe.
    
=== "Si Δ = 0"

    !!! donne "Exemple avec a = 9, b = -12, c = 4"  
        9   
        -12  
        4  
        Le résultat de l'équation est 0.6666666666666666 .

=== "Si Δ > 0"

    !!! donne "Exemple avec a = 2, b = -5, c = -3"  
        2  
        -5  
        -3  
        Les deux possibilités de x sont -0.5 et 3.0 .


??? donne "Remarques :"
    • Les solutions de l'équation ax²+bx+c=0 sont appelées racines du trinôme ax²+bx+c
    • Les solutions, lorsqu'elles existent, sont les abscisses des points d'intersection de la courbe avec l'axe des abscisse (voir tableau).  
    
    ![tableau](https://www.jeuxmaths.fr/cours/images/equasecondd.jpg)

!!! savoir  "Bonus"

    [Une 1ère vidéo en bonus](https://www.youtube.com/watch?v=youUIZ-wsYk){ .md-button }
    [Une 2ème vidéo en bonus](https://www.youtube.com/watch?v=WVYWdN13kPE){ .md-button }  
    
    <iframe width="560" height="315" src="https://www.youtube.com/embed/JvSlFtv23m4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Pour plus d'[information](https://www.maths-et-tiques.fr/index.php/cours-maths/niveau-premiere) sur les maths en première.
