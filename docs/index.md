# Solutions logicielles

Pour un apprentissage progressif, voici quelques outils à utiliser.

1. HedgeDoc ; un éditeur en ligne pour du travail collaboratif ou un document rapide à faire.
2. Carnet Jupyter ; une solution qui permet d'exécuter du code Python et d'avoir des cellules Markdown.
3. VSCodium avec une extension ; une solution performante.
4. MkDocs ; pour créer un site web avec Markdown. On peut aussi créer des macros en Python.

## Éditeur Markdown en ligne

- Il n'y a rien à installer.
- Il faut une connexion Internet.
- Il y a plusieurs bons sites qui proposent cette possibilité :
    - [StackEdit](https://stackedit.io/)
    - [Dillinger](https://dillinger.io/)
    - [HackMd](https://hackmd.io/home#) ; puis cliquer sur _**Use for free**_
    - [CodiMD](https://apps.education.fr/) ; pour les enseignants (avec connexion)
    - [HedgeDoc](https://demo.hedgedoc.org/new) ; pour les élèves (sans connexion)

On recommande fortement **HedgeDoc** pour les élèves et **CodiMd** pour les enseignants, il sera facile de partager votre création avec le lien unique de la barre d'adresse.

![Exemple HedgeDoc](./images/HedgeDoc.png)

!!! warning "Attention"
    Toute personne pourra éditer votre document dès qu'il connait le lien.

!!! tip "Astuces"
    - Un élève peut partager le lien de son travail uniquement à son professeur ; celui-ci pourra l'annoter.
    - Un groupe d'élèves peuvent se partager le lien et produire un travail collaboratif **en direct**.
    - Avec CodiMd, un enseignant peut gérer les droits de modifications ; très utile.

## Carnet Jupyter en ligne

Il est aussi possible de créer un carnet Jupyter en ligne.

- Il n'y a rien à installer.
- Il faut une connexion Internet.
- Il y a deux bonnes possibilités :
    - [Jupyter Notebook](https://notebook.basthon.fr/) proposé par Basthon.
    - Le service Capytale qui se base sur Basthon.

![Exemple Jupyter Basthon](./images/Basthon.png)

!!! warning "Attention"
    Chaque cellule Markdown devra être déclarée **Markdown**, et non **Code**.

!!! tip "Astuces"
    - Il faut penser à exécuter chaque cellule, par exemple avec ++shift+return++
    - Il sera possible d'insérer du code Python à exécuter. Un carnet est surtout pratique pour un document où on souhaite mélanger texte et code.

!!! danger "Installation de Jupyter"
    Pour une utilisation hors ligne, il est possible d'installer Jupyter sur un PC, nous ne le recommandons pas. C'est une source d'ennui potentiel pour débuter.

    Pour une utilisation hors ligne, on recommande d'utiliser VSCodium qui sait ouvrir les fichiers `.ipynb`. On peut alors disposer de la correction orthographique et grammaticale et de nombreuses facilités d'un bon éditeur de code...

## Éditeur hors ligne

Un bon éditeur de code permet d'éditer un fichier Markdown, avec l'extension `.md`.

Il existe plusieurs éditeurs de code généralistes, performants, libres et multiplateformes.

On recommande fortement [VSCodium](https://vscodium.com/).

Chercher et ajouter des extensions avec ++"CTRL"+"⇑ Maj"+X++, par exemple :

!!! tip "Markdown Preview Enhanced"
    Quand on ouvre un fichier avec l'extension `.md`, on peut alors faire un clic-droit dans le texte, il y a une entrée à cliquer dans le menu `Markdown Preview Enhanced: Open Preview to the Side Ctrl+K V`.

    On retrouve alors le côté pratique de HedgeDoc avec un panneau dans lequel on écrit et un autre avec la prévisualisation directe.

    :warning: les panneaux coulissants ne fonctionnent pas avec cette extension (le moteur Markdown n'est pas le même), mais il est assez proche. Les admonitions fonctionnent presque pareil.

!!! tip "Gremlins tracker"
    Affiche les caractères non ASCII pour éviter de mauvaises surprises.


Pour d'autres extensions et réglages, on pourra se référer à cette [page](https://lyc-84-bollene.gitlab.io/chambon/2-%C3%89diteurs/4-vscodium/)


## MkDocs

MkDocs est un générateur de site statique basé sur Python et Markdown.

Un [tutoriel](https://ens-fr.gitlab.io/mkdocs/) à part est disponible pour son installation et utilisation.

En supposant que vous l'avez déjà installé, vous pouvez faire vos premières expériences.

!!! tip "Astuces"
    On recommande d'avoir un PC avec VSCodium comme éditeur de code.
    
    :warning: Il est techniquement possible d'utiliser un smartphone sous Android, l'application Termux, et l'éditeur de code Micro. On ne recommande pas de débuter ainsi, en revanche il est possible de créer un site web depuis un téléphone ou une tablette avec cette technique. Plus tard :warning:

1. Consulter le [modèle d'expérience](https://ens-fr.gitlab.io/experience/)
2. Télécharger le squelette proposé. À décompresser dans un dossier de travail à retenir, par exemples `Documents`. Renommer le dossier obtenu en `mon_projet`, par exemple. Vérifier et **retenir** que le fichier `mkdocs.yml` se trouve bien dans le dossier `mon_projet`
3. Ouvrir VSCodium, puis menu `Fichier`, `Ouvrir le dossier... [Ctrl+K Ctrl+O]` et choisir le dossier de travail `mon_projet`
4. Avec le menu de VSCodium, `Affichage`, puis `Terminal`
5. Vérifier que la commande `dir` (pour Windows) ou `ls` (pour Linux) indique bien, entre autres, le fichier `mkdocs.yml`
6. Taper dans le terminal `mkdocs serve`
7. Ouvrir le lien proposé avec un navigateur
8. Dans le panneau explorateur de fichier de VSCodium, ouvrir le fichier `docs/index.md`
9. Modifier le fichier `index.md` pour créer une page. **À chaque sauvegarde**, on peut vérifier le résultat dans le navigateur.

À la fin de l'étape 6., le Terminal doit ressembler _approximativement_ à :

```console
$ ls
docs  LICENSE  mkdocs.yml  README.md  requirements.txt
$ mkdocs serve
INFO     -  Building documentation...
INFO     -  Cleaning site directory
INFO     -  Documentation built in 0.30 seconds
INFO     -  [00:16:10] Serving on http://127.0.0.1:8000/experience/
```

C'est ce dernier lien qu'il faut ouvrir sur son navigateur.
