# Premiers pas

Comme bon exercice, pour débuter, on recommande de choisir une page web simple que l'on souhaite recréer. On ne cherchera pas à recréer les effets de couleurs ou de disposition.

Premiers objectifs :

1. Structurer la page
2. Savoir créer des paragraphes
2. Savoir créer des listes à puces et des listes numérotées
2. Savoir insérer des liens ou des images
4. Mettre quelques rares mots en relief
5. Savoir faire un tableau

## Structurer son document

Le premier conseil à suivre et le plus important, est de structurer le document que l'on veut produire.

On utilise pour cela des sections, des sous-sections, chacune avec un titre et des paragraphes.

Voici un exemple de structure

```markdown
# Titre du document

paragraphe d'intro

## Titre du I]

### Titre du I.a)

paragraphe_1

paragraphe_2

### Titre du I.b)

paragraphes

## Titre du II]

...
```

!!! tip "Astuces"
    Il sera inutile de créer manuellement le sommaire de la page que vous recréez. Il sera possible de créer le sommaire automatiquement.
    
    :warning: Ne créez pas le sommaire manuellement :warning:

## Créer des paragraphes

:warning: Après le titre, on saute une ligne.

Il suffit ensuite d'écrire son texte, **en début de ligne**. On peut sauter à la ligne, ce sera considérer comme une espace ; le texte prendra la place disponible à l'écran de l'utilisateur.

Pour faire un nouveau paragraphe, on laisse une ligne vide entre deux paragraphes. C'est naturel.

## Créer des listes à puces

On suit le modèle

!!! note "À écrire"
    ```markdown
    Mon programme :

    - faire des titres de niveaux 1 à 6 avec #
    - faire des listes à puces
    - faire des listes numérotées
    - insérer des liens et des images
    - ajouter un peu d'emphase
    - créer des tableaux
    ```

!!! done "Résultat"

    Mon programme :

    - faire des titres de niveaux 1 à 6 avec #
    - faire des listes à puces
    - faire des listes numérotées
    - insérer des liens et des images
    - ajouter un peu d'emphase
    - créer des tableaux


## Créer une liste numérotée

On suit le modèle

!!! note "À écrire"
    ```markdown
    Solutions logicielles pour Markdown à essayer :

    1. HedgeDoc
    2. Jupyter Notebook
    3. VSCodium avec une extension
    4. MkDocs
    ```

!!! done "Résultat"

    Solutions logicielles pour Markdown à essayer :

    1. HedgeDoc
    2. Jupyter Notebook
    3. VSCodium avec une extension
    4. MkDocs

!!! tip "Astuce"
    Il suffit de commencer la numérotation à `1.`, ensuite on peut mettre les numéros de son choix. Le rendu aura la bonne numérotation. Cela permet d'avoir une très grande liste, d'ajouter au milieu ou supprimer, sans avoir à refaire la numérotation.

## Insérer un lien

On suit le modèle 

    [Texte du lien](lien)

Exemples :

!!! note "À écrire"
    ```markdown
    Solutions logicielles pour Markdown à essayer :

    1. [HedgeDoc](https://demo.hedgedoc.org/new)
    2. [Jupyter Notebook](https://notebook.basthon.fr/)
    3. [VSCodium](https://vscodium.com/) avec une extension
    4. [MkDocs](https://ens-fr.gitlab.io/mkdocs/)
    ```

!!! done "Résultat"

    Solutions logicielles pour Markdown à essayer :

    1. [HedgeDoc](https://demo.hedgedoc.org/new)
    2. [Jupyter Notebook](https://notebook.basthon.fr/)
    3. [VSCodium](https://vscodium.com/) avec une extension
    4. [MkDocs](https://ens-fr.gitlab.io/mkdocs/)

## Insérer une image

### Image sur Internet

Pour les premières expériences, on utilise une image disponible sur Internet ; il faut son url.

On suivra le modèle

    ![Texte alternatif](url)

Par exemple, sur le site [ISO Republic](https://isorepublic.com/), on trouve des images sous licences libres. On peut copier le lien de téléchargement d'une image comme celle-ci : `https://isorepublic.com/wp-content/uploads/2022/03/iso-republic-graffiti-fish-street-art-1100x574.jpg`

L'url commence par `http`, n'est pas très grande et se termine par `.jpg` ou `.png` ou `.svg`.

On peut l'insérer ainsi `![Un graffiti avec un poisson](https://isorepublic.com/wp-content/uploads/2022/03/iso-republic-graffiti-fish-street-art-1100x574.jpg)`

Ce qui donne

![Un graffiti avec un poisson](https://isorepublic.com/wp-content/uploads/2022/03/iso-republic-graffiti-fish-street-art-1100x574.jpg)

!!! warning "Important"
    1. Il faut commencer par `!`, sinon c'est juste un lien.
    2. On met un texte alternatif pour détecter une erreur d'accès à l'image, mais aussi pour aider les déficients visuels aider par des robots de lecture.
    3. On met un lien **correct** d'image. :warning: Ne pas mettre le lien d'une page où on trouve l'image. Il faut le lien **de l'image**.
    4. On verra **après** comment redimensionner l'image.
    5. Vous trouverez d'autres sites pour des images libres sur cette [page](https://linuxfr.org/news/tour-d-horizon-des-images-libres-et-pas-libres).

## Mettre quelques mots en relief

:warning: N'utiliser que rarement ces possibilités.

1. Les titres seront naturellement mis en relief. N'en n'ajouter pas !
2. Dans ce document, il y a beaucoup trop de mots en **gras** (emphase forte), vous devez en utiliser moins. Utilisez _plutôt_ l'emphase faible avec parcimonie.


On suit le modèle :

!!! note "À écrire"
    ```markdown
    Dans ce document, il y a beaucoup trop de mots en **gras** (emphase forte),
    vous devez en utiliser moins. Utilisez _plutôt_ l'emphase faible avec parcimonie.
    ```

!!! done "Résultat"

    Dans ce document, il y a beaucoup trop de mots en **gras** (emphase forte),
    vous devez en utiliser moins. Utilisez _plutôt_ l'emphase faible avec parcimonie.

## Créer un tableau

On suit le modèle :

!!! note "À écrire"
    ```markdown
    | A | B | C |
    |---|---|---|
    | Riri | Fifi | Loulou |
    | Pif | Paf | Ploum |
    ```

    Il est inutile de bien aligner les `|`.

    Il faut juste la bonne quantité.

!!! done "Résultat"

    | A | B | C |
    |---|---|---|
    | Riri | Fifi | Loulou |
    | Pif | Paf | Ploum |

## Et ensuite ?

On se réfère à [Bases de Markdown](https://ens-fr.gitlab.io/mkdocs/markdown-bases/) pour plus de détails.

Cela permet de créer une belle page avec HedgeDoc, mais aussi un document élaboré avec Jupyter.

Pour utiliser des possibilités avancées de Markdown,

- on installera MkDocs, comme indiqué sur cette [page](https://ens-fr.gitlab.io/mkdocs/)
- on utilisera VSCodium comme indiqué à la page précédente
- on expérimentera avec les possibilités présentées sur cette [page](https://ens-fr.gitlab.io/mkdocs/markdown-mkdocs/)
- on pourra s'inspirer des travaux d'élèves à la page suivante
